<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Route::get('/sync/tech-routing', 'TechRoutingController@syncTechRouting')->name('syncTechRouting');
// Route::get('/work-center-data', 'WorkCenterController@getCenterData')->name('getCenterData');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/get-com-bom', 'BomController@getComBom')->name('get-com-bom');
Route::get('/test-mo', 'TestController@testMo');
Route::get('test-bom', 'TestController@testBom');
Route::get('/get-bom-data', 'BomController@getBomData')->name('get-bom-data');
Route::get('/rest-data', 'RestTimeController@getRestTime')->name('rest-data');
Route::get('/getcenter', 'WorkCenterController@getCenter')->name('getcenter'); //get select workcenter_id
Route::get('/resource', 'ResourceController@getResource')->name('getresource');
Route::get('/rest-group', 'WorkTypeController@getRestGroup')->name('rest-group');
Route::get('/work-data', 'ProcessCalendarController@workData')->name('work-data');
Route::post('/calendar-data', 'CalendarController@calendar')->name('calendar-data');
Route::get('/workcenter', 'WorkCenterController@getIndexData')->name('getIndexData');
Route::get('/get-work-time', 'WorkTypeController@getWokrTime')->name('get-work-time');
Route::get('/organization', 'ResourceController@getOrganization')->name('getorganization');
Route::get('/work-type-data', 'WorkTypeController@getWorkTypeData')->name('work-type-data');
 //get aps-workcenter API
Route::get('/schedule-data', 'LoadScheduleController@getScheduleData')->name('schedule-data');
Route::get('/sale-order-data', 'SaleOrderController@getSaleOrderData')->name('save-sale-order');
Route::get('/current-load-data', 'SaleOrderController@getCurrentLoadedData')->name('current-data');
Route::get('/manufacture-data', 'LoadScheduleController@getManufactureData')->name('manufacture-data');
Route::get('/resourceset-getResData', 'ResourceSetController@getResData')->name('resourceset-getResData'); //get resource-set paginate
Route::post('/process-calendar-data', 'ProcessCalendarController@workCalendar')->name('process-calendar-data');
Route::get('/sync-order-result-form', 'SaleOrderController@getSynchroizedResult')->name('sync-order-result-form');
Route::get('/process-calendar-data', 'ProcessCalendarController@adjustProcessCalendar')->name('adjust-process-calendar');
Route::middleware('throttle:120')->group(function () {
    Route::get('/calendar-data', 'CalendarController@getCalnedar')->name('getcalendar');
    Route::get('/process-data', 'ProcessCalendarController@processCalendarData')->name('getprocesscalendar');
});
/* app */
Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/get-sale-order', 'api\SaleOrderController@getSaleOrder');
    Route::get('/get-manufacture', 'api\ManufactureController@getManufacture');
    Route::get('/get-next-part', 'api\ManufactureController@getNextPart');
    Route::get('/get-so-data', 'api\ManufactureController@getCurrentSatge');
    Route::get('/get-prev-manufacture', 'api\ManufactureController@getPrevMo');
    Route::get('/get-current-stage-com', 'api\ManufactureController@getCurrentStageCom');
    Route::get('/get-now-manufacture', 'api\ManufactureController@getNowManufacture');
    Route::get('/app-search-so', 'SaleOrderController@appSearchSo')->name('appSearchSo');
    Route::get('/app-search-mo', 'api\ManufactureController@appSearchMo')->name('appSearchMo');
    Route::get('/app-search-customer', 'SaleOrderController@appSearchCustomer')->name('appSearchCustomer');
    Route::get('getSubMaterialInfoV', 'api\ManufactureController@getSubMaterialInfoV'); // 製令子件資訊 API
});

/* new */
Route::get('/management', 'ManageMentController@managementIndex')->name('managementIndex');
Route::get('/assign/role', 'ManageMentController@getRole')->name('getRole');
Route::get('/assign/organization', 'ManageMentController@getOrganization')->name('getOrganization');
Route::get('/tech-routing', 'TechRoutingController@techRoutingIndex')->name('techRoutingIndex');


Route::group(['prefix' => 'auth'], function () {
    Route::get('/', 'api\AuthController@me');
    Route::post('login', 'api\AuthController@login');
    Route::post('logout', 'api\AuthController@logout');
});

Route::get('/get-resourceset', 'ResourceSetController@getResource')->name('getResource');
Route::get('/get-roleset', 'RoleSetController@getRoleSet')->name('getRoleSet'); //Role set

//吊勾參數api
Route::get('/coating-data','CoatingSetController@getCoatingData')->name('coating-data');

//異常原因API
Route::post('/abnormal','AbnormalSetController@getData')->name('abnormal-getData');

Route::get('sync-anps', 'api\AnpsController@syncANPS');
Route::post('confirm-mo','SimulationSchemeController@confirmMoStatus')->name('confirm-mo');
