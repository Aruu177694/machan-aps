<?php

namespace App\Services;

use App\Entities\Resource;
use App\Entities\SetupAnps;
use App\Entities\WorkCenter;
use App\Entities\SimulationSourceOrder;
use App\Entities\SimulationManufatureOrder;
use App\Entities\SimulationInitialScheme;
use App\Entities\SetupCoating;
use App\Repositories\SimulationSchemeRepository;

class Rest {

    public $start = 0;
    public $end = 0;

    public function __construct($start=null, $end=null) {
        $this->start = $start;
        $this->end  = $end;
    }
    public function setStart($t) {
        $this->start = $t;
    }
    public function setEnd($t) {
        $this->end = $t;
    }
}
class SimulationTransferService
{
    public function __construct(Resource $resource, SetupAnps $anps, SimulationManufatureOrder $simuMo,
                                 SimulationSchemeRepository $simuRepo, SimulationInitialScheme $simuInit, SetupCoating $setCoa, WorkCenter $workC)
    {
        $this->days = 86400;
        $this->hours = 3600;
        $this->mins = 60;
        $this->resource = $resource;
        $this->anps = $anps;
        $this->simuMo = $simuMo;
        $this->simuRepo = $simuRepo;
        $this->simuInit = $simuInit;
        $this->$setCoa = $setCoa;
        $this->workC = $workC;
        $this->aps_tmp = 1;
        $this->BK_tmp = '2018/5/1 8:00';
        $this->BL_tmp = '17:20';
        $this->BI_tmp = '1';
        $this->BM_tmp = 1;
        $this->BO_tmp = 1;
        $this->BD_tmp = 8;
        $this->BB_tmp = 53.59444;
        $this->BE_tmp = 1;
        $this->BZ_tmp = '2018/5/1 8:00';
        $this->CG_tmp = '2018/5/1 11:07';
        $this->CI_tmp = 70;
        $this->BQ_tmp = 1;
        $this->test = false;
    }

    public function generateSimulation($datas = null,$test =false)
    {
        $batch = $this->simuInit->where('created_at', '>=', date('Y-m-d'))->max('batch') ?: 0;
        $batch = $batch + 1 ;
        $org_id = '01'; // fill org
        $scheme_id = 'S'.$org_id.'-'.date('ymd-').'0'.$batch;
        $this->test = $test ;
        $stack = [];
        foreach ($datas as $key => $data) {
            // if($this->test && isset($data['CG_tmp'])) {
            //     $this->aps_tmp = $data['aps_tmp'];
            //     $this->BK_tmp = $data['BK_tmp'];
            //     $this->BL_tmp = $data['BL_tmp'];
            //     $this->BI_tmp = $data['BI_tmp'];
            //     $this->BM_tmp = $data['BM_tmp'];
            //     $this->BO_tmp = $data['BO_tmp'];
            //     $this->BD_tmp = $data['BD_tmp'];
            //     $this->BB_tmp = $data['BB_tmp'];
            //     $this->BE_tmp = $data['BE_tmp'];
            //     $this->BZ_tmp = $data['BZ_tmp'];
            //     $this->CG_tmp = $data['CG_tmp'];
            //     $this->CI_tmp = $data['CI_tmp'];
            //     $this->BQ_tmp = $data['BQ_tmp'];
            // } 
            $data['BQ'] = $this->BQ($data); // 計畫開始
            $data['BU'] = $this->BU($data); // 計畫結束
            $data['BY'] = $this->BY($data); // 建議最遲開始
            $data['CK'] = $this->CK($data); // 建議最遲完工
            $data['CD'] = $this->CD($data); // 建議最早開始
            $data['CF'] = $this->CF($data); // 建議最早完工
            $data['BK'] = $this->BK($data);
            $data['BL'] = $this->BL($data);
            $data['BI'] = $this->BI($data);
            $data['BM'] = $this->BM($data);
            $data['BO'] = $this->BO($data);
            $data['BB'] = $this->BB($data);
            $data['BE'] = $this->BE($data);
            $data['BZ'] = $this->BZ($data);
            $data['CI'] = $this->CI($data);
            $data['CG'] = $this->CG($data);
            $data['BQ'] = $this->BQ($data);
            // $datas['->BD'] );$this->->BD($data)
            if (intval($data['aps_id']/100) == 50) 
                $this->aps_tmp = 1;
            if(!$this->test){
                $data['so_id'] = $this->simuMo->where('mo_id', $data['mo_id'])->first()->so_id;
                $this->simuRepo->generateSimulation($data, $scheme_id, $batch);
            } else {
                array_push($stack ,array(
                    'BQ' => $data['BQ'] ,
                    'BU' => $data['BU'] ,
                    'BY' => $data['BY'] ,
                    'CK' => $data['CK'] ,
                    'CD' => $data['CD'] ,
                    'CF' => $data['CF'] ,
                    'KEY' =>  (String)$key,
                ));
            }
            // $this->CG_tmp = $this->CG($data);
            // $this->BK_tmp = $this->BK($data);
            // $this->BM_tmp = $this->BM($data);
            // $this->BO_tmp = $this->BO($data);
            // $this->BQ_tmp = $this->BQ($data);
            // $this->BZ_tmp = $this->BZ($data);
            $this->BK_tmp = $data['BK'];
            $this->BL_tmp = $data['BL'];
            $this->BI_tmp = $data['BI'];
            $this->BM_tmp = $data['BM'];
            $this->BO_tmp = $data['BO'];
            $this->BB_tmp = $data['BB'];
            $this->BE_tmp = $data['BE'];
            $this->BZ_tmp = $data['BZ'];
            $this->CI_tmp = $data['CI'];
            $this->CG_tmp = $data['CG'];
            $this->BQ_tmp = $data['BQ'];
            $this->aps_tmp = $data['aps_id'];
            // $this->BD_tmp = $this->BD($data);
        }
        if($this->test) return response($stack,201);
        return $scheme_id;
    }
    public function BJ($datas) //clear 排單時間單位
    {
        $AK = $this->AK($datas); // 資源標準前置期
        $BI = $this->BI($datas); // 製程時間
        if ($BI % $AK > 0) { // BI:製程時間 AK:資源標準前置期
            return (intval($BI/$AK) + 1) * $AK;
        } else {
            return intval($BI/$AK)*$AK;
        }
    }

    public function BI($datas) // clear 製程時間
    {
        $BB = $this->BB($datas);
        if ($BB % 60 > 0) {
            return intval($BB/60) + 1; // BB:總總ΣTCT(分)
        } else {
            return intval($BB/60);
        }
    }

    public function BB($datas) //clear 總ΣTCT(分)
    {
        $resource_id = $datas['resource_id'];
        $aps_id = $datas['aps_id'];
        $item_id = $datas['item_id'];

        $AJ = $datas['qty']; // 生產數量
        // $AM = $this->resource->where('resource_id', $resource_id)->first()->change_time; // 標準換線
        if($this->test){
            $AL = $datas['AL'];
            $AM = $datas['AM'];
        }
        else{ 
            $AL = $this->AL($aps_id, $item_id, $resource_id); // 標準tct
            $AM = $this->resource->where('resource_id', $resource_id)->first()->change_time; // 標準換線
        }
        $AO = $this->AO($datas, $AL, $AJ, $resource_id, $item_id); // 標準ΣTCT

        if (intval($aps_id/100) == 40) {
            return $AL * $AJ/60 + $AM; // AM:標準換線 AJ:生產數量 AL:標準tct
        } else {
            return $AO + $AM;
        }
    }

    public function AL($aps_id = null, $item_id = null , $resource_id = null) //not clear 標準tct
    {
        if ($aps_id/100 == 40) { //fix aps_id = 40
            $a = $this->setCoa::where('cu_pc_material_id', $item_id )->only('cu_tct');
            if(!$a) {
                $a = $this->resource::where('resource_id', $resource_id)->only('standard_tct');
            }
            return $a;
        } else {
            $tct = $this->anps->where('cu_material_id', $item_id)->first();
            if (!$tct) {
                $tct = $this->resource->where('resource_id', $resource_id)->first();
                return $tct->standard_tct;
            } else {
                if ($tct->cu_rw_tht == 0) {
                    $tct = $this->resource->where('resource_id', $resource_id)->first();
                    return $tct->standard_tct;
                } else {
                    return $tct->cu_rw_tht;
                }
            }
        }
    }

    public function AO($datas, $AL, $AJ, $resource_id, $item_name) //clear 標準ΣTCT fix $AN
    {
        //AL:標準tct AN:標準人數 AJ:生產數量 I:母件名稱
        // $AN = $this->resource->where('resource_id', $resource_id)->first()->device_multiple; // AN 標準人數
        if($this->test)
            $AN = $datas['AN']; 
        else $AN =18;//fix;
        $aps_id = $datas['aps_id'];
        if (intval($aps_id/100) == 50 || intval($aps_id/100) == 30) {
            return $AL/$AN*($AJ-1)/60 + $AL/60;
        }else if(intval($aps_id/100 == 40)){
            if($this->test)
                $data = strstr($datas['I'],'抽屜');
            $data = $this->simuMo->where('item_name', 'like', '%抽屜%')->first()->item_name;
            if ($data) {
                return 397.355/3 + ($AJ-1)*$AL/60;
            } else {
                return 442.055/3 + ($AJ-1)*$AL/60;
            }
        }
        else if (intval($aps_id/100) == 20) 
            return $AJ*$AL/60;
        else return $AJ*$AL/60;
    }

    public function AK($datas) // clear 資源標準前置期
    {    
        $resource_id = $datas['resource_id'];
        if($this->test) 
            return $datas['AK'];
        else return $this->resource->where('resource_id', $resource_id)->first()->standard_pre_time;
    }

    public function AT($datas) // clear 排單預計完工時間
    {
        if($this->test){
            $container_date = $datas['AS'] ;
        }
        else $container_date = $this->simuMo->where('mo_id', $datas['mo_id'])->first()->simuSo->container_date;
        return date("Y-m-d H:i", strtotime($container_date) - $this->days + 12 * $this->hours );
    }

    public function SumRestSet($ckEnd, $ckStart, $aps_id) {
        $rest = [];
        $f = 1;
        if($this->test) {
            $r1 = new Rest("10:00","10:10");
            $r2 = new Rest("12:00","13:00");
            $r3 = new Rest("15:00","15:10");
            $r4 = new Rest("17:20","17:50");
            array_push($rest,$r1,$r2,$r3);
        }
        else {
            $rest = $this->workC->where('aps_id',$aps_id)
                ->first()->resource
                ->first()->shift
                ->first()->restGroup
                ->first()->restSetup;
        }
        $BNsum = 0;
        if(is_string($ckEnd))
            $ckEnd = strtotime($ckEnd);
        if(is_string($ckStart))
            $ckStart = strtotime($ckStart);
        $ckStart = $ckStart % $this->days;
        $ckEnd = $ckEnd % $this->days;
        if($ckEnd < $ckStart) {
            $tmp = $ckEnd;
            $ckEnd = $ckStart;
            $ckStart = $tmp;
            $f = -1;
        }
        foreach ($rest as $key => $value) {
            $start = strtotime($value->start) % $this->days;
            $end = strtotime($value->end) % $this->days;
            if ( $start >= $ckStart && $start < $ckEnd)
                $BNsum += $end - $start;
        }
        return $BNsum*$f;
    }
    // public function getDay($time) {
    //     return (int)$time/$this->days;
    // }
    public function BN($datas) {
        $BK = strtotime($this->BK($datas));
        $BM = strtotime($this->BM($datas));
        $AK = $this->AK($datas);
        $BL = strtotime($this->BL($datas));
        $BB = $this->BB($datas) ;
        $BE = $this->BE($datas);

        if($this->getDate($BM) == $this->getDate($BK)){
            return $this->SumRestSet($BM,$BK,$datas['aps_id']);
        }
        else if($BK > $BM){
            if($BB/$BE/60 < $AK)
                return $this->SumRestSet($BK ,$BM, $datas['aps_id']);
            else 
                return $this->SumRestSet($BL, $BK, $datas['aps_id']) + $this->SumRestSet($BM,0,$datas['aps_id']);
        }
        else 
            return $this->SumRestSet($BL, $BK, $datas['aps_id']) + $this->SumRestSet($BM,0,$datas['aps_id']);
    }
    public function BW($datas) {
        $BU = strtotime($this->BU($datas));
        $BV = strtotime($this->BV($datas));
        $BB = $this->BB($datas);
        $AK = $this->AK($datas);
        $BV_hour = $this->getHour($BV,true);
        if( date("d",$BU) == date("d",$BV)) {
            if($BV_hour == 12)
                return $this->SumRestSet($BU, $BV,$datas['aps_id'])+60*$this->mins;
            else return $this->SumRestSet($BU, $BV ,$datas['aps_id']);
        }
        else if ($BB / 60 < $AK)
            return $this->SumRestSet($this->hours*17+$this->mins*20, $this->hours*8,$datas['aps_id'])
                -$this->SumRestSet($BV,$BU,$datas['aps_id']);
        else 
            return $this->SumRestSet($BU, $BV, $datas['aps_id']) + ($this->getDate($BU) - $this->getDate($BV))/$this->days*80*60;

        
    }
    public function BS($datas) {
        $BQ = strtotime($this->BQ($datas));
        $BR = strtotime($this->BR($datas));
        $BB = $this->BB($datas);
        $BE = $this->BE($datas);
        $AK = $this->AK($datas);
        $BL = strtotime($this->BL($datas));
        if( date('d',$BQ) == date('d',$BR))
            return ($this->SumRestSet($BR, $BQ, $datas['aps_id']) );
        else if ($BB/$BE/60 <$AK)
            return ($this->SumRestSet($BL, $BQ, $datas['aps_id'])+$this->SumRestSet($BR,0,$datas['aps_id']) );
        else return ($this->SumRestSet($BL, $BQ, $datas['aps_id'])+$this->SumRestSet($BR,0,$datas['aps_id']) );
    }
    public function getHour($time, $stramp=false) {
        if($stramp)
            $a = 1;
        else 
            $a =  $this->hours;
        if( !is_integer($time))
            $time = strtotime($time);
        if($time% $this->days+ $this->hours*8 > 24*$this->hours)
            return floor((($time%$this->days)- $this->hours*16)/$this->hours) *$a ;
        else 
            return floor((($time%$this->days)+$this->hours*8)/$this->hours)* $a;

    }
    public function getDate($time){
        if( !is_integer($time))
            $time = strtotime($time);
        if($time% $this->days < 16*$this->hours) 
            return $time - $time% $this->days - $this->hours*8;  
        else return $time - $time% $this->days - $this->hours*8 +$this->days;   
    }
    public function getMin($time, $stramp=false) {
        if($stramp)
            $a = 1;
        else 
            $a = $this->mins;
        if( !is_integer($time))
            $time = strtotime($time);
        return floor($time%$this->hours/$this->mins) *$a;
    }
    public function roundUp($v,$l) {

        $b = pow(10,$l);
        return ceil($v*$b)/$b;
    }
    public function BL($datas) {
        $BK = strtotime($this->BK($datas));
        $BD = 8; //fix
        if($BD == 8) {
            return date('H:i',$this->getDate($BK) + $this->hours*17 + $this->mins*20);
        }
        else if ($BD == 11)
            return date('H:i',$this->getDate($BK) + $this->hours*20 + $this->mins*50);
        else return date('H:i',0);
    }
    public function BK($datas) // 計畫開始時間Tmp #fix BD 模擬班別時數 BL 下班時間
    {
        $aps_id = $datas['aps_id'];
        $AP = $datas['cu_ush_date']; // 預設出貨日期
        $AK = $this->AK($datas);
        $BJ = $this->BJ($datas); // 時間排單單位
        $BD = 8;
        // $shift = $this->workC->where('aps_id',$datas['aps_id'])
        //     ->first()->resource
        //     ->first()->shift;
        // $rest = $shift->restGroup->first()->restSetup;
        // $total_rest_time = 0
        // foreach ($rest as $key => $value) 
        //   $total_rest_time += strtotime($value->end) - strtotime($value->start) ;
        // $shift_time = strtotime($shift->work_off) - strtotime($shift->work_on) ;
        // $BD = ($shift_time - $total_rest_time) / $this->hours ;
        $d = strtotime(date("Y-m-d", strtotime($this->BK_tmp)));
        if (intval($aps_id/100) == 50) { // BD:模擬班別時數 BJ:時間排單單位 AK:資源標準前置期 AP:預設出貨日期 #130
            if ($BJ/$AK%2 == 1) {
                if (intval($BJ/$BD) > 0) {

                    return date("Y-m-d H:i", strtotime($AP) - intval($BJ/$BD + 1) * $this->days + 13 * $this->hours);
                } else {
                    return date("Y-m-d H:i", strtotime($AP) - $this->days + 8 * $this->hours);
                }
            } else {
                return date("Y-m-d H:i", strtotime($AP) - intval(($BJ/$BD) + 1) * $this->days + 8 * $this->hours);
            }
        } else if(intval($this->aps_tmp/100) != intval($aps_id/100)) {
            if (intval($BJ/$AK)%2 == 1) {
                if (date("H", strtotime($this->BK_tmp)) == 8) {
                    if (intval($BJ/$AK)%2 == 1) {

                        return date("Y-m-d H:i", $d - intval($BJ/$BD + 1) * $this->days + 13 * $this->hours);
                        // date("Y-m-d H:i:s", strtotime(YEAR(BK3),MONTH(BK3),DAY(BK3) - intval(BJ4/BD4 + 1) * $day) + 13 * 60 * 60));
                    } else {
                        return date("Y-m-d H:i", $d - $this->days + 8 * $this->hours);
                        // date("Y-m-d H:i:s", strtotime(YEAR(BK3),MONTH(BK3),DAY(BK3) - $day + 8 * 60 * 60));
                    }
                } else {
                    return date("Y-m-d H:i", $d - intval($BJ/$BD + 1) * $this->days + 13 * $this->hours);
                    // date("Y-m-d H:i:s", strtotime(YEAR(BK3),MONTH(BK3),DAY(BK3) - intval(BJ4/BD4) + 1) * $day + 8 * 60 * 60));
                }
            } else {
                return date("Y-m-d H:i", $d - intval($BJ/$BD) * $this->days + 8 * $this->hours);
            }
        } else { //BK3 date("H", strtotime(BK3))
            if (date("H:i", strtotime($this->BK_tmp) + $this->BI_tmp * $this->hours) > $this->BL_tmp) {
                // date("H:i:s", strtotime(BK3) + BI3 > date("H", strtotime(BL3)) ) {
                    return date("Y-m-d H:i", $d - $this->days + 8 * $this->hours);
                // date("Y-m-d H:i:s", strtotime(YEAR(BK3),MONTH(BK3),DAY(BK3) - $day + 8 * 60 * 60));
            } else {
                return date("Y-m-d H:i", strtotime($this->BK_tmp) + $this->BI_tmp * $this->hours);
                // date("H:i:s", strtotime(BK3) + BI3
            }
        }
    }

    public function BM($datas) // clear 計畫開始完工tmp fix $BL 下班時間
    {
        $BB = $this->BB($datas);
        $BE = $this->BE($datas);
        $BK = $this->BK($datas);
        $BL = $this->BL($datas); // resource relation setup_shift work_off
        if (date("H:i:s", strtotime($BK) + ($BB/$BE) * $this->mins)  < date("H:i", strtotime($BL))) {
            return date("Y-m-d H:i", strtotime($BK) + ($BB/$BE) * $this->mins);
        } else {
            return date("Y-m-d H:i", strtotime(date("Y-m-d", strtotime($BK) + $this->days)) + 8 * $this->hours + intval($BB/$BE) * $this->mins);
        }
    }

    public function BE($datas) // clear 設備人力倍數
    {
        if ($this->test) {
            return $datas['BE'];
        } else 
            return $this->resource->where('resource_id', $datas['resource_id'])->first()->device_multiple;
    }

    public function BO($datas) // clear 計畫完工時間tmp
    {
        $BM = $this->BM($datas);
        $BN = $this->BN($datas); // 一休
        if (strtotime($BM) + $BN  > strtotime(date("Y-m-d", strtotime($BM))) + 17 * $this->hours + 20 * $this->mins) {
            return date("Y-m-d H:i", strtotime(date("Y-m-d", strtotime($BM))) + $this->days + strtotime(date("H:i", strtotime($BM)))
                + $BN + 8 * $this->hours - (17 * $this->hours + 20 * $this->mins) - strtotime(date("Y-m-d")));
        } else {
            if (date("H:i", strtotime($BM) + $BN ) == "12:00") {
                return date("Y-m-d H:i", strtotime($BM) + $BN + 60* $this->mins);
            }
            else {
                if (date("H:i", strtotime($BM)) == "10:00" && date("H:i", strtotime($BM) + $BN) < "10:10") {
                    return date("Y-m-d H:i", strtotime($BM) + $BN );
                } else {
                    if (date("H:i", strtotime($BM)) == "15:00" && date("H:i", strtotime($BM) + $BN) < "15:10") {
                        return date("Y-m-d H:i", strtotime($BM) + $BN);
                    } else {
                        return date("Y-m-d H:i", strtotime($BM) + $BN);
                    }
                }
            }
        }
    }

    public function BQ($datas) // clear 計畫開始時間 fix $BD 下班時間
    {
        $aps_id = $datas['aps_id'];
        $BK = $this->BK($datas);
        $BM = $this->BM($datas);
        $BJ = $this->BJ($datas);
        $BO = $this->BO($datas);
        $BD = 8; //fix

        if (intval($this->aps_tmp/100) != intval($aps_id/100)) {

            return date("Y-m-d H:i", strtotime($BK));
        } else {
            if (strtotime($BM) - strtotime($BK) < 0) {
                return date("Y-m-d H:i", $this->getDate($BO) + $this->getHour($BO)
                    + $this->roundUp($this->getMin($BO,true),-1)*$this->mins);
            } else {
                if (strtotime($this->BM_tmp) - strtotime($this->BK_tmp) < 0) {
                    return date("Y-m-d H:i", strtotime($BK));
                } else {
                    if (intval($BJ/$BD) > 0) {
                        return date("Y-m-d H:i", $this->getDate($BO) 
                            + $this->getHour($BO)
                            + $this->roundUp($this->getMin($BO,true),-1)*$this->mins);
                    } else {
                        if ( strtotime($this->BO_tmp) > $this->getDate($this->BO_tmp) + 17*$this->hours + 20*$this->mins) {
                            return date("Y-m-d H:i", $this->getDate($BM) 
                                + $this->getHour($BM)
                                + $this->roundUp($this->getMin($BM,true),-1)*$this->mins);
                        } else {
                            if ($this->getDate($BK) != $this->getDate($this->BK_tmp)) {

                                return date("Y-m-d H:i", strtotime($BK));
                            } else {
                                return date("Y-m-d H:i", $this->getDate($this->BO_tmp) 
                                    + $this->getHour($this->BO_tmp)
                                    + $this->roundUp($this->getMin($this->BO_tmp,true),-1)*$this->mins);
                            }
                        }
                    }
                }
            }
        }
    }

    public function BR($datas) // clear 計畫結束時間tmp fix 休息時間 BD 班別
    {
        $BQ = $this->BQ($datas);
        $BB = $this->BB($datas);
        $BI = $this->BI($datas);
        $BD = 8;
        $BE = $this->BE($datas);
        $BN = $this->BN($datas); //fix 休息時間
        if (date("Y-m-d H:i:s", strtotime($BQ) + intval($BB/$BE)*60+$BN) 
            > date("Y-m-d H:i:s", strtotime(date("Y-m-d", strtotime($BQ))) + 17 * $this->hours + 20 * $this->mins)) {
            return date("Y-m-d H:i", 
                strtotime(date("Y-m-d", strtotime($BQ))) 
                + intval($BI/$BD+1) * $this->days + 8 * $this->hours 
                + intval(($BB/$BE) % ($BD * 60)) * $this->mins 
                - (strtotime(date("Y-m-d", strtotime($BQ))) + 17 * $this->hours + 20 * $this->mins - strtotime($BQ)));
        } else {
            return date("Y-m-d H:i", strtotime($BQ) + intval($BB/$BE) * $this->mins);
        }
        /* if (date("Y-m-d H:i:s", strtotime("YEAR(BQ4),MONTH(BQ4),DAY(BQ4),H,i,s + (BB4/BE4+BN4) mins")) > date("Y-m-d H:i:s", strtotime("YEAR(BQ4),MONTH(BQ4),DAY(BQ4) + 17 hours + 20 mins"))) {
            date("Y-m-d", strtotime("BQ4 + (BI4/BD4+1) days + 8 hours + ((BB4/BE4) % (BD4 *60)) mins ") - (INT(BQ4)+TIME(17,20,0)-BQ4)
            INT(BQ4)+INT(BI4/BD4)+1+TIME(8,0,0)+TIME(0,MOD(BB4/BE4,BD4*60),0)-(INT(BQ4)+TIMEO(17,20,0)-BQ4)
        } else {
            date("Y-m-d, strtotime("BQ4 + (BB4/BE4) mins")")
            BQ4+TIME(0,BB4/BE4,0)
        } */
    }

    public function BU($datas) // clear 計畫結束時間
    {

        $BR = $this->BR($datas);
        if(strtotime($BR) > $this->getDate($BR) +12*$this->hours){ 
            return date("Y-m-d H:i", $this->getDate($BR)+17*$this->hours + 20*$this->mins);     
        }
        else {
            return date("Y-m-d H:i", $this->getDate($BR) + 12 * $this->hours);
        }
        // if($BR > $this->getDate($BR)+$this->hours*17+$this->mins*20)
        //     return date("Y:m:d H:i",$this->getDate($BR)+$this->hours*17+$this->mins*20);
        // else return date("Y:m:d H:i",$this->getDate($BR)+$this->hours*12);
    }

    public function BV($datas) // clear 建議最遲開始tmp fix BD 模擬班別時數
    {
        $BU = $this->BU($datas);
        $BB = $this->BB($datas);
        $BD = 8;
        $BE = $this->BE($datas);
        $BI = $this->BI($datas);
        $AK = $this->AK($datas);
        if (date("H", strtotime($BU)) == '17') {
            if ( $BB/$BD/60 < 1) {

                return date ("Y-m-d H:i", strtotime($BU) - intval($BB/$BE) * $this->mins);
                //date("Y-m-d H:i:s", strtotime("BU4 - (BB4/BE4) mins"))
            } else {
                return date("Y-m-d H:i",  strtotime($BU) - intval($BI/$BD) * $this->days + 8 * $this->hours - ($BB/$BE) * $this->mins );
                //date("Y-m-d H:i:s", strtotime("BU4 - (BI4/BD4) days + 8 hours - (BB4/BE4) mins"))
            }
        } else {
            if ($BB/60/$AK < 1) {
                return date("Y-m-d H:i", strtotime($BU) - intval($BB/$BE) * $this->mins);
            } else {
                return date("Y-m-d H:i", strtotime(date("Y-m-d", strtotime($BU))) - intval(1+$BI/$BD) * $this->days 
                    + 17 * $this->hours +20 *$this->mins - (intval($BB/$BE)-240) * $this->mins );
            }
        }
    }    

    public function BY($datas) // clear 建議最遲開始時間 fix BW 休息時間
    {
        $BV = $this->BV($datas);
        $BW = $this->BW($datas);
        if (date("H", strtotime($BV)) == '12') {
            return date("Y-m-d H:i", strtotime($BV) - $BW -60 * $this->mins);
        } 
        else {
            if ((date("H", strtotime($BV) - $BW) == '10') && (date("H:i", strtotime($BV) - $BW) < '10:10')) {
                return date("Y-m-d H:i", strtotime($BV) - $BW - 10* $this->mins);
            } else {
                if ((date("H", strtotime($BV) - $BW) == '15') && (date("H:i", strtotime($BV) - $BW) < '15:10')) {
                    return date("Y-m-d H:i", strtotime($BV) - $BW-10 * $this->mins);
                } else {
                    return date("Y-m-d H:i", strtotime($BV) - $BW);
                }
            }
        }
    }

    public function BZ($datas) // 建議最早開始時間tmp fix BZ BB BD BI BE tmp
    {
        $aps_id = $datas['aps_id'];
        $BQ = $this->BQ($datas);
        $BD_tmp = 8; #BD
        if (intval($aps_id/100) == 50) 
            return date("Y-m-d H:i", strtotime($BQ));
        else if (strtotime($this->BZ_tmp) ? false : true) 
                return 'error BZ';
        else if (
            date("Y-m-d H:i", strtotime($this->BZ_tmp) 
            - intval($this->BB_tmp/$this->BE_tmp) * $this->mins) < 
            date("Y-m-d H:i", strtotime(date("Y-m-d", strtotime($this->BZ_tmp))) 
            + 8 * $this->hours) ||
            date("Y-m-d H:i", strtotime($this->BZ_tmp) 
            - intval($this->BB_tmp/$this->BE_tmp) * $this->mins) > 
            date("Y-m-d H:i", strtotime(date("Y-m-d", strtotime($this->BZ_tmp))) 
            + 17 * $this->hours + 20 * $this->mins)
            ) {
                return date("Y-m-d H:i", strtotime(date("Y-m-d", strtotime($this->BZ_tmp))) 
                    - $this->days 
                    + $this->hours*17 + $this->mins*20
                    // - intval(1+($this->BI_tmp/$this->BD_tmp)) * $this->days 
                    // + (17-8) * $this->hours 
                    - ((intval($this->BB_tmp/$this->BE_tmp) % ($this->BD_tmp*60)) * $this->mins 
                    - (strtotime($this->BZ_tmp) - (strtotime(date("Y-m-d", strtotime($this->BZ_tmp)))+8*$this->hours)))
                    -intval($this->BI_tmp/$this->BD_tmp));
            
        }
        else return date("Y-m-d H:i",strtotime($this->BZ_tmp) - intval($this->BB_tmp/$this->BE_tmp)*$this->mins);
    }

    public function CD($datas) // clear 建議最早開始時間 fix CB 休息時間
    {
        $aps_id = $datas['aps_id'];
        $BZ = $this->BZ($datas);
        $BQ = $this->BQ($datas);
        $CB = $this->CB($datas);
        if ($BZ == 'error BZ') {
            return 'error BZ';
        } else if (intval($aps_id/100) == 50) {
            return date("Y-m-d H:i", strtotime($BQ));
        } else {
            if (date("H", strtotime($BZ) - $CB) == '12') {
                return date("Y-m-d H:i", strtotime($BZ) - $this->hours - $CB);
            } else {
                if (date("H", strtotime($BZ) - $CB) == '15') {
                    return date("Y-m-d H:i", strtotime($BZ) - ($CB+10*$this->mins));
                } else {
                    if (date("H", strtotime($BZ) - $CB) < '8') {
                        return date("Y-m-d H:i", strtotime(date("Y-m-d", strtotime($BZ))) 
                            - $this->days + 17 * $this->hours + 20* $this->mins - abs($CB)
                            +(strtotime($BZ) -($this->getDate($BZ)+8*$this->hours)));
                    } else {

                        return date("Y-m-d H:i", strtotime($BZ) - $CB);
                    }
                }
            }
        }
    }
    public function CA($datas) {
        $BZ = $this->BZ($datas);
        $BD  = 8; // #BD
        if($BD == 8) {
            return date("H:i", $this->getDate($BZ)+ 17*$this->hours + 20*$this->mins);
        }
        else if ($BD == 11) {
            return date("H:i", $this->getDate($BZ)+ 20*$this->hours + 50*$this->mins);
        }else {
            return 0;
        }
    }
    public function CB($datas) {
        $CF = $this->CF($datas);
        $BZ = $this->BZ($datas);
        $CA = $this->CA($datas);
        if(intval($datas['aps_id']/100) == 50) {
            if($this->getDate($CF) == $this->getDate($BZ)){
                return $this->SumRestSet($CF,$BZ,$datas['aps_id']);
            } else {
                return $this->SumRestSet($BZ,$CF,$datas['aps_id']);
            }
        } 
        else if($this->getDate($BZ) == $this->getDate($this->BZ_tmp)) 
            return $this->SumRestSet($this->BZ_tmp,$BZ,$datas['aps_id']);
        else {
            return $this->SumRestSet($CA,$BZ,$datas['aps_id'])
                +$this->SumRestSet($this->BZ_tmp,0,$datas['aps_id']);
        }
    }

    public function CF($datas) // clear 建議最早完工時間 fix BS 休息時間
    {
        $BS = $this->BS($datas);
        $BR = $this->BR($datas);
        if (date("H", strtotime($BR)) == '12') {
            return date("Y-m-d H:i", strtotime($BR) + $BS + 60 * $this->mins);
        } else {
            if (date("H", strtotime($BR) + $BS) == '10' && date("H:i", strtotime($BR) + $BS - $this->getDate($BR)) < '10:10') {
                return date("Y-m-d H:i", strtotime($BR) + $BS + 10* $this->mins);
            } else {
                if ((date("H", strtotime($BR) + $BS ) == '15') && (date("H", strtotime($BR) + $BS - $this->getDate($BR)) < '15:10')) {
                    return date("Y-m-d H:i", strtotime($BR) + $BS + 10 * $this->mins);
                } else {
                    return date("Y-m-d H:i", strtotime($BR) + $BS);
                }
            }
        }
        /* if (date("H", strtotime("BR4")) == '12) {
            date("Y-m-d H:i", strtotime("BR4 + (BS4+60) mins))
        } else {
            if (date("H", strtotime("BR4 - BS4 mins")) == '10' && date("H:i", strtotime("BR4 + BS4 mins")) < '10:10') {
                date("Y-m-d H:i", strtotime("BR4 + (BS4+10) mins"))
            } else {
                if (date("H", strtotime("BR4 - BS4 mins")) == '15' && date("H:i", strtotime("BR4 + BS4 mins")) < '15:10') {
                    date("Y-m-d H:i", strtotime("BR4 + (BS4+10) mins"))
                } else {
                    date("Y-m-d H:i", strtotime("BR4 + BS4 mins"))
                }
            }
        }*/
    }

    public function CG($datas) //clear  建議最遲完工tmp fix BD 模擬班別
    {
        $aps_id = $datas['aps_id'];
        $BU = $this->BU($datas);
        $BB = $this->BB($datas);
        $BE = $this->BE($datas);
        $BI = $this->BI($datas);
        $BD = 8;
        if (intval($aps_id/100) == 50) {
            if (date("H", strtotime($BU) - ($BB/$BE) * $this->mins) < '8') {
                return date("Y-m-d H:i", strtotime(date("Y-m-d", strtotime($BU))) - $this->days + 8 * $this->hours + intval(($BB/$BE)%240) * $this->mins);
            } else {

                return date("Y-m-d H:i", strtotime($BU) - intval($BB/$BE) * $this->mins);
            }
        } else {
            if (strtotime($this->CG_tmp) - ($BB/$BE) * $this->mins > 
                $this->getDate($this->CG_tmp) + 8 * $this->hours) {
                return date("Y-m-d H:i", strtotime($this->CG_tmp) - intval($BB/$BE) * $this->mins);
            } else {
                return date("Y-m-d H:i", 
                    $this->getDate($this->CG_tmp) 
                    - (1+intval($BI/8)) * $this->days + 17 * $this->hours +20*$this->mins 
                    - (intval(($BB/$BE) % ($BD*60)) * $this->mins 
                    - (strtotime($this->CG_tmp)- $this->getDate($this->CG_tmp)- 8 * $this->hours)));
            }
        }
    }

    public function CK($datas) // clear 建議最遲完工時間
    {
        $aps_id = $datas['aps_id'];
        $AT = $this->AT($datas);

        if (intval($aps_id/100) == 50) {
            return date("Y-m-d H:i", strtotime($AT));
        } else {
            if (date("H", strtotime($this->CG_tmp)) == '12') {
                return date("Y-m-d H:i", strtotime($this->CG_tmp) - $this->CI_tmp - 60* $this->mins);
            } else {
                if (date("H", strtotime($this->CG_tmp) - $this->CI_tmp) < '8') {
                    return date("Y-m-d H:i", 
                        $this->getDate($this->CG_tmp)- $this->days + 17*$this->hours + 20*$this->mins
                        -(strtotime($this->CG_tmp) - ($this->getDate($this->CG_tmp) + 8*$this->hours)));
                } else {
                    return date("Y-m-d H:i", strtotime($this->CG_tmp) - $this->CI_tmp);
                }
            }
        }
    }
    public function CH($datas){
        $BD = 8;
        $CG = $this->CG($datas);
        if($BD == 8)
            $result = date("H:i",$this->getDate($CG) + $this->hours*17 + $this->mins*20);
        else if($BD == 11)
            $result = date("H:i",$this->getDate($CG) + $this->hours*20 + $this->mins*50);
        else $result = 0;
        return $result;
    }
    public function CI($datas){
        $CH = $this->CH($datas);
        $CG = $this->CG($datas);
        if(intval($datas['aps_id']/100) == 50)
            $result = $this->SumRestSet($CH ,$CG,$datas['aps_id']);
        else if ($this->getDate($CG) == $this->getDate($this->CG_tmp))
            $result = $this->SumRestSet($this->CG_tmp,$CG,$datas['aps_id']) ;
        else {
            $result = $this->SumRestSet($CH ,$CG,$datas['aps_id']) +$this->SumRestSet($this->CG_tmp ,0,$datas['aps_id']) ;
        }
        return $result;
    }
}
