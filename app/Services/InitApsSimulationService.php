<?php

namespace App\Services;

/*撰寫人: 鄧臣宏*/
    /*
        excel 時戳以天為單位
        php   時戳以秒為單位

    */


//此一時間格式運算之function，用以模仿Excel 之時間運算

function T($unit, $val) 
{
    //$unit 時間單位 
    //$val  運算值

    if ($unit === 'hour') 
    {
        $time = 3600*$val;
    }
    else if ($unit === 'minute') 
    {
        $time = 60*$val;
    }
    else if ($unit === 'second') 
    {
        $time = $val;
    }
    else if ($unit === 'day') {
        $time = $val*3600*24;
    }
    return $time;
}

//切出時戳點鐘 INT(X) -X
function T_CutDot($date)
{
    return $date - T_CutDate($date) ;
}
//切出時戳日期 INT(X)
function T_CutDate($date) {
    $date = date('Ymd',$date);
    $date = strtotime($date);
    // $date = floor($date/(3600*24))*3600*24;
    return $date;

}
//將a增加或減少至最接近b的倍數
function mRound($a, $b) {
    return floor($a/$b) *$b ;
}
function HOUR($date) {
    return (int)date('H', $date);

}
function MINUTE($date) {
    return (int)date('i', $date);
}
function ROUNDUP($var, $i) {
    $var =$var * pow(10, $i) ;
    $var = floor($var)+1;
    $var = $var / pow(10, $i);
    return $var;
}

class InitApsSimulationService 
{
	/*
	Q 	: 製程階
	AT 	: 排單預計完工時間(點鐘)
	CG 	: 建議最遲完工時間 tmp(點鐘)
	CI 	: 休息時間CI
	*/

    //建議最遲完工時間 --CK

	public function latestFinishTime($Q, $AT, $CG, $CI) 
	{

		if  ($Q == 50) 
		{
			$result = $AT;

		}
		else if  (date("H", $CG) == 12 ) 
		{
			$result = $CG - T('minute', -$CI+60);
		}
		else if  (date("H", $CG + T('minute', -$CI)) < 8)
		{
        	//INT(B1) - 1 + TIME(17, 20, 0)
            $temp1 = T_CutDate($CG);
            $temp1 = $temp1 - T('day', 1);
            $temp1 = $temp1 + T('hour', 17);
            $temp1 = $temp1 + T('minute', 20);
        	//CG - INT(CG) + TIME(8, 0, 0) 
            $temp2 = T_CutDot($CG);
            $temp2 = $temp2 - T('hour', 8);
			$result = $temp1 - $temp2   ;

		}
		else {

			$result = $CG - T('minute', $CI);
		}

		return $result;
		
	}
    //建議最遲完工tmp --CG
    public function latestFinishTimeTmp($Q, $BB,$BD, $BE, $BI, $BU, $CG)
    {
        if  ($Q == 50)
        {
            if  (date("H", $BU - T('minute', ($BB/$BE))) < 8)
            {
                $result = T_CutDate($BU) - T('day', 1) + T('hour', 8) 
                    + T('minute', ($BB/$BE) % 240);

            }
            else {
                $result = $BU - T('minute',$BB/$BE);
            }
        }else if  (!$temp['CG']) {
            $result = false;
        }else if  ($temp['CG'] - T('minute', $BB/$BE) > T_CutDate($temp['CG']) + T('hour', 8)) {
            $result = $temp['CG'] - T('minute', $BB/$BE) ;
        }else {
            $result = T_CutDot($CG) - T('day', 1) +T('hour', 17) + T('minute', 20) - floor($BI/8) -(T('minute', ($BB/$BE) % ($BD*60) - (T_CutDot($temp['CG']) - T('hour', 8)))) ;
        }
        return $result;
    }
 
    //初設出貨日期 --AP
    //AS 結關日期 ; AE 出貨日期
    public function defaultShippingDate($AE, $AS) {

        if ($AE == "") {
            $result = $AS;
        }else {
            $result =$AE;
            // $result = date("Y/m/d", strtotime($AE));
        }

        return $result;
    }

    //結關日期 --AS
    //$AF 結觀日期字串
    public function customsDate($AF) {
        if ($AF = "") {
            $result = "";
        }else {
            $result = date("Y/m/d", strtotime($AF));
        }

        return $result;
    }
    // 排單預計完工日 --AT
    //$AS 結關日期
    public function scheduledCompletionDate($AS) {
        return $AS - T('day', 1) + T('hour', 12);
    }

    //標準總TCT --AO (可能放在repository)
    //Q 製程階; AJ生產數量;AN標準人數 ;AL標準TCT
    // public function standardTotalTCT($Q, $AJ, $AL, $AN, $sql='') {
    //     // I? H?
    //     if  ($Q==50 || $Q==30) {
    //         return $AL/$AN*($AJ-1)/60+$AL/60; //AL:標準tct AN:標準人數 AJ:生產數量 I:母件名稱
    //     } else if  ($Q==20 || $Q==10){
    //         return $AJ*$AL/60;
    //     } else {
    //         // $sql = SimulationMo::where('item_name',  'like',  '%抽屜%')->only('item_name');
    //         if  ($sql) {
    //             return 397.355/3+($AJ-1)*$AL/60;
    //         } else {
    //             return 442.055/3+($AJ-1)*$AL/60;
    //         }
    //     }
    // }

    //標準TCT --AL (可能放在repository)
    // public function stardardTCT() {
    //     if  (Q4==40) {
    //         $a = 塗裝釣鉤參數::where('母件',  $母件)->only(tct);
    //         if (!$a) {
    //             $a = 資源設定::where('資源中心代碼',  $資源中心代碼)->only('tct');
    //         }
    //         return $a;
    //     } else {
    //         $a = ANPS::where('母件',  $母件)->only('tht');
    //         if  (!$a) {
    //             $a = 資源設定::where('資源中心代碼',  $資源中心代碼)->only('tct');
    //         }
    //         return $a;
    //     }
    // }

    //計畫結束時間 --BU
    //BR 計畫結束時間tmp
    public function planEndTime($BR) {
        if ($BR > T_CutDate($BR) + T('hour', 12) ) {
            $result = T_CutDate($BR) + T('hour', 17) + T('minute', 20);
        }else {
            $result = T_CutDate($BR) + T('hour', 12);
        }
        return $result;
    }

    //計畫結束時間tmp --BR
    public function planEndTimeTmp($BB, $BE, $BQ) { 
        if ($BQ + T('minute', $BB/$BE) > T_CutDate($BQ)+T('hour', 17)+T('minute', 20)){
            $result = T_CutDate($BQ) + floor($BI/$BD)+T('day', 1)+T('hour', 8)
                +T('minute', ($BB/$BE) % ($BD*60)) - ( T_CutDate($BQ)+T('hour', 17)+T('minute', 20)-$BQ);
        }else {
            $result = $BQ + T('minute', $BB/$BE);
        }
        return $result;
    }

    //總TCT --BB
    public function totalTCT($Q, $AJ, $AL, $AM, $AO) {
        if ($Q == 40) {
            $result = $AL*$AJ/60 +$AM;
        }else {
            $result = $AO +$AM;
        }
        return $result;
    }
    //製程 --BI
    public function processTimeBucket($BB) {
        if ($BB%60 > 0) {
            $result = floor($BB/60)+1;
        }else {
            $result = floor($BB/60);
        }
        return $result;
    }

    //當天下班時間 --BL
    public function offWorkTime($BD, $BK) {
        if  ($BD == 8) {
            $result = T_CutDate($BK) + T('hour',17)+T('minute',20);
        } else if  ($BD == 11) {
            $result = T_CutDate($BK) + T('hour',20)+T('minute',25);
        } 
        return $result;
    }

    //時間單位Time --BJ
    public function timeBucketHR($AK, $BI) {
        if ($BI % $AK > 0) {
            $result = floor($BI/$AK) +1 * $AK;
        }else {
            $result = mRound($BI, $AK);
        }
        return $result;
    }

    //計畫開始時間--BQ
    public function planStartTime($Q, $BD, $BJ, $BK, $BM, $BO,$temp) { //Q ,BK,BM,BO has former and current;
        if ($temp['Q'] != $Q) {
            $result = $BK;
        } else if ( $BM - $BK < 0) {
            $result = T_CutDate($BO) +T('hour', HOUR($BO)) + T('minute', ROUNDUP(MINUTE($BO), -1)) ;
        } else if ($temp['BM'] - $temp['BK'] <0) {
            $result = $BK;
        } else if  (floor($BJ/$BD) > 0) {
            $result = T_CutDate($BO) +T('hour', HOUR($BO)) + T('minute', ROUNDUP(MINUTE($BO), -1)) ;
        } else if ($temp['BO'] > T_CutDate($temp['BO'])+T('hour', 17)+T('minute', 20)) {
            $result = T_CutDate($BM)+T('hour', HOUR($BM))+T('minute', ROUNDUP(MINUTE($BM), -1));
        } else if (T_CutDate($BK) != T_CutDate($temp['BK'])) {
            $result = $BK ;
        } else {
            $result = T_CutDate($temp['BO'])+ T('hour', HOUR($temp['BO'])) + T('minute', ROUNDUP(MINUTE($temp['BO']), -1));
        }
        return $result;
    }

    //計畫開始完工時間 --BM
    public function startingEndTimeOfPlan($BB, $BE, $BK, $BI, $BD, $BL) {
        if ($BK + T('minute',$BB/$BE) < $BL) {
            $result = $BK + T('minute', $BB/$BE);
        }else {
            $result = T_CutDate($BK)+floor($BI/$BD)+T('hour', 8)+T('minute', ($BB/$BE) % ($BD*60)-($BL - $BK));
        }
        return $result;
    }

    //計畫開始時間tmp --BK
    public function startingEndTimeOfPlanTmp($Q,$AK,$AP,$BD,$BJ,$temp) {
        if ($Q == 50) { // BD:模擬班別時數 BJ:時間排單單位 AK:資源標準前置期 AP:預設出貨日期 #130
                        // BK,BI,BL 是前一筆
            if ($BJ/$BD % 2 == 1) {
                if ($BJ/$BD > 0) {
                    $result = $AP - T('day',floor($BJ/$BD +1)) + T('hour',13);
                } else {
                    $result = $AP -T('day',1) + T('hour',8);
                }
            }else {
                $result = $AP - T('day',floor($BJ/$BD)+1) + T('hour',8);
            }
        }else if ($temp['Q']!=$Q) { 
            if (($BJ/$BD)%2 == 1) {
                if (HOUR($temp['BK']) == 8) {
                    if (($BJ/$BD)%2 == 1) {
                        $result = $temp['BK'] - T('day',floor($BJ/$BD +1)) + T('hour',13);
                    } else {
                        $result = $temp['BK'] - T('day',1) +T('hour',8);
                    }
                }else {
                    $result = $temp['BK'] - T('day',floor($BJ/$BD +1)) + T('hour',8);
                }
            }else {
                if ($temp['BK'] +T('hour',$BI) >$BL) { 
                    $result = $temp['BK'] -T('day',1)+T('hour',8);
                }else {
                    // date("H:i:s", strtotime($BK) + $BI
                    $result = $temp['BK'] + T('hour',$BI);
                }
            }
        }
        return $result;
    }

    //計畫完工時間tmp --BO
    public function planFinishTimeTmp($AT, $BJ, $BM, $BN) {
        if ($BM+T('minute',$BN) > T_CutDate($BM)+T('hour',17)+T('minute',20)) {
            $result = T_CutDate($BM)+T('day',1)+floor($BJ/$AT)+T('hour',8)+$BM+T('minute',$BN)-(T_CutDate($BM)+T('hour',17)+T('minute',20));

        }else if (HOUR($BM+T('minute',$BN))==12) {
            $result = $BM +T('minute',$BN+60);
        }else if (HOUR($BM+T('minute',$BN))==10&&$BM+T('minute',$BN)-T_CutDate($BM) < T('hour',10)+T('minute',10)) {
            $result = $BM+T('minute',$BN+10);
        }else if (HOUR($BM+T('minute',$BN))==15&&$BM+T('minute',$BN)-T_CutDate($BM) < T('hour',15)+T('minute',10)) {
            $result = $BM+T('minute',$BN+10);
        }else {
            $result = $BM+T('minute',$BN);
        }
        return $result;

    }
/*
IF (
    HOUR(BV4)=12, BV4-TIME(,BW4+60,), IF (
        AND(HOUR(BV4-TIME(,BW4,))=10,BV4-TIME(,BW4,)-INT(BV4)< TIME(10,10,0)), BV4-TIME(,BW4+10,), IF (
            AND(HOUR(BV4-TIME(,BW4,))=15, BV4-TIME(,BW4,)-INT(BV4)< TIME(15,10,0)), BV4-TIME(,BW4+10,), BV4-TIME(,BW4,))
    )
*/
    //最遲開始時間 --BY
    public function latestStartTime($BV,$BW) {
        if (HOUR($BV) == 12) {
            $result = $BV -T('minute',$BW+60);
        } else if (HOUR($BV-T('minute',$BW))==10&&$BV-T('minute',$BW)-T_CutDate($BV) < T('hour',10) + T('minute',10)) {
            $result = $BV - T('minute', $BW+10);
        } else if (HOUR($BV - T('minute',$BW))==15&&$BV-T('minute',$BW)-T_CutDate($BV) < T('hour',15)+T('minute',10) ){
            $result = $BV-T('minute',$BW+10);
        }else{
            $result = $BV -T('minute',$BW);
        }
        return $result;

    }
/*
=IF(HOUR(BR4)=12,
    BR4+TIME(,BS4+60,),
    IF(AND(HOUR(BR4+TIME(,BS4,))=10,
        BR4+TIME(,BS4,)-INT(BR4)<TIME(10,10,0)),BR4+TIME(,BS4+10,),
        IF(AND(HOUR(BR4+TIME(,BS4,))=15,BR4+TIME(,BS4,)-INT(BR4)<TIME(15,10,0)),
        BR4+TIME(,BS4+10,),BR4+TIME(,BS4,))))
*/
    //建議最早完工時間 --CF
    public function earliestFinishTime($BR,$BS) {
        if(HOUR($BR) == 12) {
            $result = $BR + T('minute',$BS+60);
        } else if (HOUR($BR+T('minute',$BS))==10&&$BR+T('minute',$BS)-T_CutDate($BR)<T('hour',10)+T('minute',10)) {
            $result = $BR + T('minute',$BS+10);
        } else if (HOUR($BR+T('minute',$BS))==15&&$BR+T('minute',$BS)-T_CutDate($BR)<T('hour',15)+T('minute',10)) {
            $result = $BR + T('minute',$BS+10);
        } else {
            $result = $BR + T('minute',$BS);
        }
        return $result;
    }

/*
IF (
    Q4=50, BQ4,IF (
        HOUR(BZ4-TIME(,ABS(CB4),))=12, BZ4-TIME(1,ABS(CB4),), IF (
            AND(HOUR(BZ4-TIME(,ABS(CB4),))=15,INT(BZ4)*TIME(15,10,0)+BZ4-TIME(,ABS(CB4),)>0), BZ4-TIME(,ABS(CB4)+10,), IF (
                HOUR(BZ4-TIME(,ABS(CB4),))< 8, INT(BZ4)-1+TIME(17,20,0)-(TIME(,ABS(CB4),)-(BZ4-(INT(BZ4)+TIME(8,0,0)))), BZ4-TIME(,ABS(CB4),)
            )
        )
    )
*/
    //建議最早開始時間 --CD 
    public function earliestStartTime($Q,$BQ,$BZ,$CB) {
        if ($Q == 50) {
            $result = $BQ ;
        } else if (HOUR($BZ-T('minute',abs($CB)))==15) {
            $result = $BZ-T('hour',1)-T('minute',abs($CB));
        } else if (HOUR($BZ-T('minute',abs($CB)))==15&&T_CutDate($BZ)*(T('hour',15)+T('minute',10))+$BZ-T('minute',abs($CB)) >0) {
            $result = $BZ -T('minute',abs($CB)+10);
        } else if (HOUR($BZ-T('minute',abs($CB)))<8) {
            $result = T_CutDate($BZ)-T('day',1)+T('hour',17)+T('minute',20)-T('minute',abs($CB))-($BZ-T_CutDate($BZ)+T('hour',8));
        } else {
            $result = $BZ - T('minute',abs($CB));
        }

        return $result;
    }

/*
IF (
    Q4=50, 
    BQ4, 
        IF (
            BZ3-TIME(,BB3/BE3,)< INT(BZ3)+TIME(8,0,0), 
            INT(BZ3)-1+TIME(17,20,)-(TIME(,MOD(BB3/BE3,BD3*60),)-(BZ3-(INT(BZ3)+TIME(8,0,0))))-INT(BI3/BD3), 
                IF (
                    BZ3-TIME(,BB3/BE3,)>INT(BZ3)+TIME(17,20,0), 
                    INT(BZ3)-1+TIME(17,20,)-(TIME(,MOD(BB3/BE3,BD3*60),)-(BZ3-(INT(BZ3)+TIME(8,0,0))))-INT(BI3/BD3), 
                    BZ3-TIME(,BB3/BE3,)
        )
    )
*/
    //建議最早開始時間tmp --BZ
    public function earliestStartTimeTmp($Q,$BQ,$temp) {
        if($Q == 50) {
            // dd(date('Y/m/d H:i',$BQ));
            $result = $BQ;
        } else if ($temp['BZ']-T('minute',$temp['BB']/$temp['BE'])<T_CutDate($temp['BZ'])+T('hour',8)) {
            $result = T_CutDate($BZ)-T('day',1)
                +T('hour',17)+T('minute',20)
                -(T('minute',($temp['BB']/$temp['BE'])%($temp['BD']*60))
                -(T_CutDot($temp['BZ'])+T('hour',8)))
                -T_CutDate($temp['BI']/$temp['BD']);
        } else if ($temp['BZ']-T('minute',$temp['BB']/$temp['BE'])>T_CutDate($temp['BZ'])+T('hour',17)+T('minute',20)) {
            $result = T_CutDate($temp['BZ'])-T('day',1)
                +T('hour',17)+T('minute',20)
                -(T('minute',$temp['BB']/$temp['BE']%($temp['BD']*60))
                -(T_CutDot($temp['BZ'])-T('hour',8)))
                -T_CutDate($temp['BI']/$temp['BD']);
        } else {
            $result = $temp['BZ'] - T('minute',$temp['BB']/$temp['BE']);
        }
        return $result;
    }
/*
IF (
    HOUR(BU4)=17, IF (
        BB4/60/BD4< 1, BU4-TIME(,BB4/BE4,), BU4-TIME(0,BB4/BE4,0)-INT(BI4/ BD4)+TIME(8,0,0)
    ), IF (
        BB4/60/AK4< 1, BU4-TIME(,BB4/BE4,), INT(BU4)-1-TIME(0,BB4/BE4-240,0)-INT(BI4/BD4)+TIME(17,20,0)
    )
*/
    //最遲開始時間tmp --BV
    public function latestStartTimeTmp($AK,$BB,$BD,$BE,$BU,$BI) {
        if (HOUR($BU)==17) {
            if ($BB/60/$BD <1) {
                $result = $BU -T('minute',$BB/$BE);
            }
            else {
                $result = $BU -T('minute',$BB/$BE)-T_CutDate($BI/$BD)+T('hour',8);
            }
        } else if ($BB/60/$AK <1) {
            $result = $BU-T('minute',$BB/$BE);
        } else {
            $result = T_CutDate($BU)-T('day',1)-T('minute',$BB/$BE-240)-T_CutDate($BI/$BD)+T('hour',17)+T('minute',20);
        }
        return $result;
    }
    //data : (AF/AS), AJ , AE,BD, AM   AO,  AK, BE, AL ,CI ,BN ,temp[...]
    public function operate($data,$temp="") {
        if(!$temp) {
            $temp = [
                'CG' => 0,
                'BK' => 0,
                'BM' => 0,
                'BO' => 0,
                'Q' =>  0,
                'BB' => 0,
                'BE' => 0,
                'BZ' => 0,
                'BI' => 0,

            ];
        }
        // $AO = standardTotalTCT($data['Q'], $data['AJ'], $data['AL'], $data['AN'], $data['AO_sql']);
        $BB = $this->totalTCT($data['Q'], $data['AJ'], $data['AL'], $data['AM'], $data['AO']);
        $BI = $this->processTimeBucket($BB);
        $AT = $this->scheduledCompletionDate($data['AS']) ;
        $BI = $this->processTimeBucket($BB) ;
        $BJ = $this->timeBucketHR($data['AK'], $BI);
        $AP = $this->defaultShippingDate($data['AE'], $data['AS']);
        $BK = $this->startingEndTimeOfPlanTmp($data['Q'], $data['AK'], $AP, $data['BD'], $BJ, $temp);
        $BL = $this->offWorkTime($data['BD'], $BK);
        $BM = $this->startingEndTimeOfPlan($BB, $data['BE'], $BK, $BI, $data['BD'], $BL);
        $BO = $this->planFinishTimeTmp($AT, $BJ, $BM, $data['BN']);
        $BQ = $this->planStartTime($data['Q'], $data['BD'], $BJ, $BK, $BM, $BO,$temp) ;
        $BR = $this->planEndTimeTmp($BB, $data['BE'], $BQ);
        $BU = $this->planEndTime($BR);
        $CG = $this->latestFinishTimeTmp($data['Q'], $BB, $data['BD'], $data['BE'], $BI, $BU, $temp['CG1']);
        $CK = $this->latestFinishTime($data['Q'], $AT, $CG, $data['CI']);
        $BZ = $this->earliestStartTimeTmp($data['Q'],$BQ,$temp);
        $BV = $this->latestStartTimeTmp($data['AK'],$BB,$data['BD'],$data['BE'],$BU,$BI);
        $BY = $this->latestStartTime($BV,$data['BW']);
        $CF = $this->earliestFinishTime($BR,$data['BS']);
        $CD = $this->earliestStartTime($data['Q'],$BQ,$BZ,$data['CB']);

        $result = [
            // "standardTotalTCT" => $AO,
            "totalTCT" => $BB,
            "processTimeBucket" => $BI,
            "scheduledCompletionDate" => date('Y/m/d H:i', $AT),
            "timeBucketHR" => $BJ,
            "defaultShippingDate" => date('Y/m/d', $AP),
            "startingEndTimeOfPlanTmp" => date('Y/m/d H:i', $BK),
            "offWorkTime" => date('H:i',$BL), //~!
            "startingEndTimeOfPlan" => date('Y/m/d H:i', $BM), 
            "planFinishTimeTmp" => date('Y/m/d H:i', $BO), 
            "planStartTime" => date('Y/m/d H:i' ,$BQ),
            "planEndTimeTmp" => date('Y/m/d H:i' ,$BR),
            "planEndTime" => date('Y/m/d H:i' ,$BU), 
            "latestFinishTimeTmp" => date('Y/m/d H:i' ,$CG), // ~!
            "latestFinishTime" => date('Y/m/d H:i' ,$CK),
            "earliestStartTimeTmp" => date('Y/m/d H:i',$BZ),
            "latestStartTimeTmp" => date('Y/m/d H:i',$BV),
            "latestStartTime" => date('Y/m/d H:i',$BY),
            "earliestFinishTime" => date('Y/m/d H:i',$CF),
            "earliestStartTime" => date('Y/m/d H:i',$CD),
            "temp" => [
                'CG' =>$CG,
                'BK' =>$BK,
                'BM' => $BM,
                'BO' => $BO,
                'Q' => $data['Q'],
                'BB' => $BB,
                'BE' => $data['BE'],
                'BZ' => $BZ,
                'BI' => $BI,
            ]
        ];
        return $result;

    }


}
