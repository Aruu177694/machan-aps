<?php

namespace App\Services;

use Carbon\Carbon;
use Ixudra\Curl\Facades\Curl;
use App\Entities\Schedule;
use App\Repositories\ManufactureRepository;
use App\Repositories\SaleOrderRepository;
use App\Repositories\SimulationSchemeRepository;
use App\Repositories\BomRepository;

class GetProduceService
{
    private $apiServer;
    private $bodyStart;
    private $bodyEnd;
    private $strConnecting;
    private $strData;
    protected $orderRepo;
    protected $manufactureRepo;
    protected $bomRepo;
    protected $simulationRepo;

    public function __construct(SaleOrderRepository $orderRepo, ManufactureRepository $manufactureRepo,
                                BomRepository $bomRepo, SimulationSchemeRepository $simulationRepo)
    {
		$this->apiServer = 'http://'.env('MACHAN_API').'/WebService/CAPInteropServiceEx.asmx';
        $this->bodyStart = '<?xml version="1.0" encoding="utf-8" ?>'.
            '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" '.
            'xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.
            'xmlns:xsd="http://www.w3.org/2001/XMLSchema"><soap:Body><ExecuteProc xmlns="http://tempuri.org/">';
		$this->bodyEnd = '<ucoInvoke>false</ucoInvoke><logMode>0</logMode></ExecuteProc></soap:Body></soap:Envelope>';
        $this->strConnecting = '<groupId>'.env('GROUP_ID').
            '</groupId><language/>zh-TW<language/><userId>'.env('USER_ID').'</userId><password>'.env('PASSWORD').'</password>';
		$this->strData = [
            '</ExecuteProcResult></ExecuteProcResponse></soap:Body></soap:Envelope>',
            '<?xml version="1.0" encoding="utf-8"?><soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/"'.
            ' xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">'.
            '<soap:Body><ExecuteProcResponse xmlns="http://tempuri.org/"><ExecuteProcResult xsi:type="xsd:string">'
        ];
        $this->orderRepo = $orderRepo;
        $this->manufactureRepo = $manufactureRepo;
        $this->bomRepo = $bomRepo;
        $this->simulationRepo = $simulationRepo;
    }

    private function connectWebService($params)
    {
        $response = Curl::to($this->apiServer)
            ->withData($this->bodyStart.$params.$this->bodyEnd)
            ->withContentType('text/xml')
            ->post();

        foreach ($this->strData as $key => $string) {
            $response = str_replace($string, '', $response);
        }

        return json_decode($response);
    }

    public function getSourceOrder($orgId, $billStartDate, $billEndDate, $conStartDate, $conEndDate, $soId, $customerName)
    {
        $conStartDate = $conStartDate ? ' and A.CU_ContainerDate3 &gt;='.Carbon::parse($conStartDate)->format('Ymd') : '';
        $conEndDate = $conEndDate ? ' and A.CU_ContainerDate3 &lt;='.Carbon::parse($conEndDate)->format('Ymd') : '';
        $billStartDate = $billStartDate ?? Carbon::createFromDate(now()->year, now()->month, 1, 'Asia/Taipei')->format('Ymd');
        $billEndDate = $billEndDate ?? Carbon::createFromDate(now()->year, now()->month, now()->daysInMonth, 'Asia/Taipei')->format('Ymd');
        $customerName = $customerName ? " and A.BizPartnerName='$customerName'" : '';
        // if ($soId) {
        //     $arrData = explode(',', $soId);
        //     $dataStr = 'and (';
        //     foreach ($arrData as $key => $value) {
        //         if ($key) {
        //             $dataStr .= ' or ';
        //         }
        //         $dataStr .= "A.BillNo='$value'";
        //     }
        //     $dataStr .= ')';
        // }
        if ($soId) {
            $soData = explode(',', $soId);
        }
        foreach ($soData as $key => $so) {
            $params = $this->strConnecting.'<progId>ppProduceOrder</progId><methodName>GetDataFromInterface</methodName>'.
                '<wParams><anyType xsi:type="xsd:string">salSalesOrder</anyType>'.
                '<anyType xsi:type="xsd:string">A.CurrentState,A.TypeId,A.OrgId,A.BillNo,A.BillDate,A.BizPartnerId,'.
                    'A.BizPartnerName,A.CustomerOrderNo,B.RowId,B.RowNo,B.MaterialId,B.MaterialName,B.RequirementDate,'.
                    'B.CU_MOTransfer,B.CU_ScheStatus,B.BPMaterialId, A.CU_USHdate, A.CU_ContainerDate3, B.SQuantity, A.PersonId, B.MaterialSpec, B.SUnitId, B.UnTransSQty, B.CU_Remark2</anyType>'.
                '<anyType xsi:type="xsd:string">A.BillDate &gt;= '.$billStartDate.' and A.BillDate &lt;= '.$billEndDate.' and A.OrgId='.$orgId.$conStartDate.$conEndDate.' and A.BillNo='."'".$so."'".$customerName.'</anyType>'. // fix and A.CurrentState=2
                '<anyType xsi:type="xsd:boolean">true</anyType></wParams>';
            $result = $this->connectWebService($params);
            if (!$result) {
                Schedule::where('so_id', $so)->update(['sync_check' => '2']);
            } else {
                Schedule::where('so_id', $so)->update(['sync_check' => '1']);
                $this->orderRepo->synchronize($result);
            }
        }
        // $params = $this->strConnecting.'<progId>ppProduceOrder</progId><methodName>GetDataFromInterface</methodName>'.
        //     '<wParams><anyType xsi:type="xsd:string">salSalesOrder</anyType>'.
        //     '<anyType xsi:type="xsd:string">A.CurrentState,A.TypeId,A.OrgId,A.BillNo,A.BillDate,A.BizPartnerId,'.
        //         'A.BizPartnerName,A.CustomerOrderNo,B.RowId,B.RowNo,B.MaterialId,B.MaterialName,B.RequirementDate,'.
        //         'B.CU_MOTransfer,B.CU_ScheStatus,B.BPMaterialId, A.CU_USHdate, A.CU_ContainerDate3, B.SQuantity, A.PersonId, B.MaterialSpec, B.SUnitId, B.UnTransSQty, B.CU_Remark2</anyType>'.
        //     '<anyType xsi:type="xsd:string">A.BillDate &gt;= '.$billStartDate.' and A.BillDate &lt;= '.$billEndDate.' and A.OrgId='.$orgId.$conStartDate.$conEndDate.' '.($dataStr ?? '').$customerName.'</anyType>'. // fix and A.CurrentState=2
        //     '<anyType xsi:type="xsd:boolean">true</anyType></wParams>';
        // $result = $this->connectWebService($params);
        // return $this->orderRepo->synchronize($result);
    }

    public function getManufactureOrder($data, $batch)
    {
        $params = $this->strConnecting.'<progId>ppProduceOrder</progId><methodName>GetDataFromInterface</methodName>'.
            '<wParams><anyType xsi:type="xsd:string">ppProduceOrder</anyType>'.
            '<anyType xsi:type="xsd:string">A.TypeId,A._P_A1_FactoryId,A.BillNo,A.BillDate,A.BizPartnerId,A.BizPartnerName'.
                ',A.FromBillNo, A.MaterialId,A.ProduceQty,A.UnitId,A.FromTechRouteKeyId,A.MaterialName,A.DemandBeginDate,A.DemandCompleteDate,'.
                'A.DemandStockInDate,A.ParentBillNo,A.ProduceState,A.CurrentState</anyType>'.
            '<anyType xsi:type="xsd:string">A.TypeId="MO10" and A.FromBillNo="'.$data->so_id.'"</anyType>'.
            '<anyType xsi:type="xsd:boolean">true</anyType></wParams>';
        $result = $this->connectWebService($params);
        $this->manufactureRepo->synchronize($result, $batch);
        return $data->item_id;
    }

    public function getBom($data, $item_id, $getBom = null)
    {
        $params = $this->strConnecting.'<progId>X_WebServiceInterface</progId><methodName>GetDataFromInterface</methodName>'.
    	'<wParams><anyType xsi:type="xsd:string">ppBOM</anyType>'.
        '<anyType xsi:type="xsd:string">A.MaterialId,A.BOMKeyId,A.BOMKeyName,A.UnitId,A.TechRouteKeyId,E.RowNo,E.RowId,E.SubMaterialId'.
            ',E.BaseQty,E.UnitQty,E.NUseQty,E.UnitId,E.Remark,E.FetchType</anyType>'.
            '<anyType xsi:type="xsd:string">A.BOMKeyId="'.$data.'"</anyType></wParams>';
        $result = $this->connectWebService($params);
        return $this->bomRepo->synchronize($result, $item_id, $getBom);
    }

    public function getComBom($data)
    {
        $params = $this->strConnecting.'<progId>X_WebServiceInterface</progId><methodName>GetDataFromInterface</methodName>'.
    	'<wParams><anyType xsi:type="xsd:string">ppBOM</anyType>'.
        '<anyType xsi:type="xsd:string">A.MaterialId,A.BOMKeyId,A.BOMKeyName,A.UnitId,A.TechRouteKeyId,E.RowNo,E.RowId,E.SubMaterialId'.
            ',E.BaseQty,E.UnitQty,E.NUseQty,E.UnitId,E.Remark,E.FetchType</anyType>'.
            '<anyType xsi:type="xsd:string">A.BOMKeyId="'.$data.'"</anyType></wParams>';
        return $this->connectWebService($params);
    }

    public function getSourceOrderData($orgId, $billStartDate, $billEndDate, $conStartDate, $conEndDate, $soId, $customerName) //simulation so
    {
        $conStartDate = $conStartDate ? ' and A.CU_ContainerDate3 &gt;='.Carbon::parse($conStartDate)->format('Ymd') : '';
        $conEndDate = $conEndDate ? ' and A.CU_ContainerDate3 &lt;='.Carbon::parse($conEndDate)->format('Ymd') : '';
        $billStartDate = $billStartDate ?? Carbon::createFromDate(now()->year, now()->month, 1, 'Asia/Taipei')->format('Ymd');
        $billEndDate = $billEndDate ?? Carbon::createFromDate(now()->year, now()->month, now()->daysInMonth, 'Asia/Taipei')->format('Ymd');
        $customerName = $customerName ? " and A.BizPartnerName='$customerName'" : '';
        if ($soId) {
            $arrData = explode(',', $soId);
            $dataStr = 'and (';
            foreach ($arrData as $key => $value) {
                if ($key) {
                    $dataStr .= ' or ';
                }
                $dataStr .= "A.BillNo='$value'";
            }
            $dataStr .= ')';
        }
        /* #150 APSState fix checkbox */
        $params = $this->strConnecting.'<progId>ppProduceOrder</progId><methodName>GetDataFromInterface</methodName>'.
            '<wParams><anyType xsi:type="xsd:string">salSalesOrder</anyType>'.
            '<anyType xsi:type="xsd:string">A.CurrentState,A.TypeId,A.OrgId,A.BillNo,A.BillDate,A.BizPartnerId,'.
                'A.BizPartnerName,A.CustomerOrderNo,B.RowId,B.RowNo,B.MaterialId,B.MaterialName,B.RequirementDate,'.
                'B.CU_MOTransfer,B.CU_ScheStatus,B.BPMaterialId, A.CU_USHdate, A.CU_ContainerDate3, B.SQuantity, A.PersonId, B.MaterialSpec, B.SUnitId, B.UnTransSQty, B.CU_Remark2</anyType>'.
            '<anyType xsi:type="xsd:string">A.BillDate &gt;= '.$billStartDate.' and A.BillDate &lt;= '.$billEndDate.' and A.OrgId='.$orgId.$conStartDate.$conEndDate.' and A.CurrentState=2 '.($dataStr ?? '').$customerName.' </anyType>'.
            '<anyType xsi:type="xsd:boolean">true</anyType></wParams>';
        $result = $this->connectWebService($params);
        return $this->simulationRepo->syncSourceOrder($result);
    }

    public function getManufactureOrderData(array $datas) //simulation mo
    {
        $emptySo = []; //determine so_id whether is empty
        foreach ($datas as $key => $data) {
            $params = $this->strConnecting.'<progId>ppProduceOrder</progId><methodName>GetDataFromInterface</methodName>'.
                '<wParams><anyType xsi:type="xsd:string">ppProduceOrder</anyType>'.
                '<anyType xsi:type="xsd:string">A.TypeId,A._P_A1_FactoryId,A.BillNo,A.BillDate,A.BizPartnerId,A.BizPartnerName'.
                    ',A.FromBillNo, A.MaterialId,A.ProduceQty,A.UnitId,A.FromTechRouteKeyId,A.MaterialName,A.DemandBeginDate,A.DemandCompleteDate,'.
                    'A.DemandStockInDate,A.ParentBillNo,A.ProduceState,A.CurrentState</anyType>'.
                '<anyType xsi:type="xsd:string">A.TypeId="MO10" and A.FromBillNo="'.$data.'"</anyType>'.
                '<anyType xsi:type="xsd:boolean">true</anyType></wParams>';
            $result = $this->connectWebService($params);
            if (!empty($result)) {
                $this->simulationRepo->syncManufactureOrder($result);
            } else {
                array_push($emptySo, $data); //check so non mo
            }
        }
    }

    public function testMo($so_id)
    {
        $params = $this->strConnecting.'<progId>ppProduceOrder</progId><methodName>GetDataFromInterface</methodName>'.
            '<wParams><anyType xsi:type="xsd:string">ppProduceOrder</anyType>'.
            '<anyType xsi:type="xsd:string">A.TypeId,A._P_A1_FactoryId,A.BillNo,A.BillDate,A.BizPartnerId,A.BizPartnerName'.
                ',A.FromBillNo, A.MaterialId,A.ProduceQty,A.UnitId,A.FromTechRouteKeyId,A.MaterialName,A.DemandBeginDate,A.DemandCompleteDate,'.
                'A.DemandStockInDate,A.ParentBillNo,A.ProduceState,A.CurrentState</anyType>'.
            '<anyType xsi:type="xsd:string">A.TypeId="MO10" and A.FromBillNo="'.$so_id.'"</anyType>'.
            '<anyType xsi:type="xsd:boolean">true</anyType></wParams>';
        $result = $this->connectWebService($params);
        return $result;
    }
}
