<?php

namespace App\Services;

use App\Repositories\UserRepository;
use App\Repositories\WorkCenterRepository;
use App\Repositories\TechRoutingRepository;
use App\Repositories\AnpsRepository;
use App\Entities\ParentPart;
use Ixudra\Curl\Facades\Curl;

class MachanService
{
    private $apiServer;
    protected $userRepo;
    protected $workcenterRepo;
    protected $techRoutingRepo;

    public function __construct(UserRepository $userRepo, WorkCenterRepository $workcenterRepo,
                                TechRoutingRepository $techRoutingRepo, AnpsRepository $anpsRepo)
    {
        $this->apiServer = 'http://'.env('MACHAN_JSON_API');
        $this->userRepo = $userRepo;
        $this->workcenterRepo = $workcenterRepo;
        $this->techRoutingRepo = $techRoutingRepo;
        $this->anpsRepo = $anpsRepo;
    }

    private function connectWebService($uri, $params = [])
    {
        if (!env('MACHAN_JSON_API')) {
            throw new Exception('MACHAN_JSON_API not defined');
        }

        return Curl::to($this->apiServer.$uri)
            ->withData($params)
            ->get();
    }

    public function syncPerson()
    {
        $personInfo = $this->connectWebService('/api/PersonV');
        $this->userRepo->create(json_decode($personInfo));
    }

    public function getChildBom($datas)
    {
        foreach ($datas as $key => $data) {
            $params = json_decode($this->connectWebService('/api/MaterialGroupV/'.$data->material_id));
            if ($params) {
                ParentPart::updateOrCreate(
                    ['material_id' => $params->MaterialId],
                    [
                        'bomkey_name' => $params->MaterialName,
                    ]
                );
            } else {
                // api no data wait machan provide but not effect
            }
            
        }
    }

    public function getWorkCenter() //sync workcenter
    {
        $uri = '/api/WorkCenterV';
        $result = $this->connectWebService($uri);
        if(!$result) return 0;
        $data = $this->workcenterRepo->getData(json_decode($result));
        return 1;
    }

    public function syncTechRouting()
    {
        $uri = '/api/TechRouteV';
        $result = $this->connectWebService($uri);
        if(!$result) return 0;
        $this->techRoutingRepo->syncTechRouting(json_decode($result));
        return 1;

    }

    public function getCoating() //塗裝釣鉤
    {
        $uri = '/api/SetupCoatingV';
        $result = $this->connectWebService($uri);
        $data = json_decode($result);
        return $data;
    }

    public function syncANPS() // sync ANPS
    {
        $uri = '/api/ANPSV';
        $result = $this->connectWebService($uri);
        $data = $this->anpsRepo->syncANPS(json_decode($result));

        return $data;
    }

    public function getSubMaterialInfoV($mo_id = null)
    {
        if ($mo_id) {
            $uri = '/api/SubMaterialInfoV?'.'$filter=%20BillNo%20eq%20%27'.$mo_id.'%27';
            return $this->connectWebService($uri);
        } else {
            return 'no mo_id';
        }
    }
}

