<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\InitApsSimulationService;
use App\Entities\Resource;
class InitApsSimulationController extends Controller
{
    protected $initSimu ;
    public function __construct(InitApsSimulationService $initSimu) {
    	$this->initSimu = $initSimu;
    }

    public function testOperation() {
        $temp = "";
        $variable[0] ="";
        foreach ($variable as $key => $value) {
            $data = [
                'AS' => strtotime('20180502'),
                'AJ' => 40,
                'AE' => strtotime('20180502'),
                'Q' => 50,
                'BD' => 8,
                'AO' => 43.595,
                'AM' => 10,
                'AK' => 4,
                'BE' => 1,
                'AL' => 826,
                'CI' => 70,
                'BN' =>  0,
                'BW' => 0,
                'BS' => 0,
                'CB' => 0,

            ];
            $result = $this->initSimu->operate($data,$temp);
            $temp =$result['temp'];
            dd($result,$temp);
        }
    	// $data = [
    	// 	'AS' => strtotime('20180502'),
    	// 	'AJ' => 40,
    	// 	'AE' => strtotime('20180502'),
    	// 	'Q' => 50,
    	// 	'BD' => 8,
    	// 	'AO' => 43.595,
    	// 	'AM' => 10,
    	// 	'AK' => 4,
    	// 	'BE' => 1,
    	// 	'AL' => 826,
    	// 	'CI' => 70,
    	// 	'BN' =>  0,

    	// ];
    	// $result = $this->initSimu->operate($data);

    }
}
