<?php

namespace App\Http\Controllers;

use App\Http\Requests\ImportExcel;
use Maatwebsite\Excel\Facades\Excel;
use App\Entities\Schedule;
use App\Entities\SaleOrder;
use App\Entities\Manufacture;
use App\Entities\Organization;
use App\Entities\ParentPart;
use App\Entities\ChildPart;
use App\Entities\Resource;
use App\Entities\ProcessCalendar;
use App\Entities\CompanyCalendar;
use App\Services\GetProduceService;
use App\Services\MachanService;
use Carbon\Carbon;
set_time_limit(0);
ini_set('memory_limit', '256M');
class LoadScheduleController extends Controller
{
    public function __construct(GetProduceService $produceService, MachanService $machanService)
    {
        $this->produceService = $produceService;
        $this->machanService = $machanService;
        $this->comResults = '';
        $this->errorMo = [];
        $this->result = [];
    }

    public function index()
    {
        return view('dataload/load-schedule');
    }

    public function scheduleResult()
    {
        return view('dataload/load-schedule-result');
    }

    public function manufacture()
    {
        $maxBatch = Schedule::max('batch');
        $datas = Schedule::where('batch', $maxBatch)->get(['so_id', 'item_id']);
        $batch = Manufacture::max('batch') ?: 0;
        foreach ($datas as $key => $data) {
            $item_id = $this->produceService->getManufactureOrder($data, ($batch + 1));
            $this->getBomData($item_id);
        }
        $errorMo = $this->ScheduleDate();
        return view('dataload/load-manufacture-order', [ 'errorMo' => $errorMo]);
    }

    public function getBomData($item_id)
    {
        $getBom = $this->produceService->getBom($item_id, $item_id);
        while ($getBom) {
            $getBom = $this->produceService->getBom($getBom[0]->material_id, $item_id, $getBom);
            array_splice($getBom,0 ,1);
        }
        $childBom = ParentPart::where('fetch_type', 0)
                ->whereNull('bomkey_name')->get();
        $result = $this->machanService->getChildBom($childBom);
    }

    public function getManufactureData()
    {
        $maxBatch = Manufacture::max('batch');
        $result = Manufacture::where('batch', $maxBatch)
            ->with('relatedTechRoute')
            ->orderBy('so_id')
            ->orderBy('complete_date', 'asec');
        return response()->json($result->paginate(request()->amount));
    }

    public function importFile(ImportExcel $request)
    {
        $this->comResults = [];
        $data = request(['org_id', 'process_type', 'date', 'file']);
        $month = explode('-', $data['date'])[1];
        Excel::selectSheets((int)$month.'月生產進表')->load($data['file'], function($reader) use ($data) { // determine schedule month
            $results = $reader->select(['預計上線日', '產線別', '訂單號碼 (雷)', '品號 (沖)'])->get();
            $batch = Schedule::max('batch') ?: 0;
            foreach ($results as $key => $result) {
                if ($result['預計上線日'] && $result['產線別'] == "A" && $result['訂單號碼 (雷)']) {
                    array_push($this->result, $result);
                    $date = $data['date'].'-'.$result['預計上線日'];
                    Schedule::updateOrCreate(
                        [
                            'date' => $date,
                            'org_id' => $data['org_id'],
                            'so_id' => explode(' ', $result['訂單號碼 (雷)'])[0],
                            'item_id' => explode(' ', $result['品號 (沖)'])[0],
                        ],
                        [
                            'batch' => $batch + 1,
                        ]
                    );
                }
            }
        });
        foreach ($this->result as $key => $result) { // change check status & push search so to array
            $check = Schedule::where('so_id', explode(' ', $result['訂單號碼 (雷)'])[0])->first();
            if(!$check) {
            } else if($check['sync_check'] == 0) {
                array_push($this->comResults, $check->so_id);
            }
        }
        if (!$this->comResults) {
            return redirect()->route('load-schedule-result');
        } else {
            return redirect()->route('sale-order')->with(['comResults' => json_encode($this->comResults), 'org_id' => $data['org_id']]);
        }
    }

    public function getScheduleData()
    {
        $maxBatch = Schedule::max('batch');
        $result = Schedule::where('batch', $maxBatch)->where('sync_check', '1'); // get sync success
        return response()->json($result->paginate(request()->amount));
    }

    private function ScheduleDate()
    {
        $routingMapping = [
            '1' => 4,
            '2' => 3,
            '3' => 2,
            '4' => 1,
            '5' => 0
        ];
        $maxBatch = Schedule::max('batch');
        $datas = Schedule::where('batch', $maxBatch)->get(['so_id', 'item_id', 'date']);
        foreach ($datas as $key => $data) {
            $moDatas = Manufacture::where('so_id', $data->so_id)
                ->get()
                ->map(function ($row) use ($data) {
                    try {
                        $row->child = $row->relatedParentPart->relatedChild()->where('item_id', $data->item_id)->get();
                        return $row;
                    } catch (\Throwable $th) {
                        array_push($this->errorMo, $row->mo_id);
                    }
                })
                ->sortBy(function ($row) {
                    if ($row) {
                        return $row->child->first->level;
                    }
                })
                ->values()
                ->all();

            Manufacture::where('item_id', $data->item_id)
                ->where('so_id', $data->so_id)
                ->update(['complete_date' => $data->date]);

            foreach ($moDatas as $key => $moData) {
                if ($moData && $moData->child->isNotEmpty()) {
                    $subDay = $routingMapping[substr($moData->relatedTechRoute->aps_id, 0, 1)];
                    unset($moData->child);

                    $date = Carbon::parse($data->date);
                    while ($subDay > 0) {
                        $date->subDay();
                        $workCenterId = $moData
                            ->relatedTechRoute
                            ->workCenter
                            ->workcenter_id;
                        $resource = Resource::where('workcenter_id', $workCenterId)
                            ->where('is_default', 1)
                            ->first();
                        $company = CompanyCalendar::whereDate('date', $date)
                            ->first();
                        if ($resource != null) {
                            $processData = ProcessCalendar::where('resource_id', $resource->id)
                                ->whereDate('date', $date)
                                ->first();
                            if ($processData != null) {
                                if ($processData->status == '1') {
                                    $subDay--;
                                }
                            } elseif ($company != null) {
                                if ($company->status == '1') {
                                    $subDay--;
                                }
                            } elseif ($date->format('D') != 'Sat' && $date->format('D') != 'Sun') {
                                $subDay--;
                            }
                        } else {
                            if ($company != null) {
                                if ($company->status == '1') {
                                    $subDay--;
                                }
                            } elseif ($date->format('D') != 'Sat' && $date->format('D') != 'Sun') {
                                $subDay--;
                            }
                        }
                    }
                    $moData->update(['complete_date' => Carbon::parse($date)]);
                }
            }
        }
        return $this->errorMo;
    }
}
