<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchSaleOrder;
use App\Services\GetProduceService;
use App\Services\SimulationTransferService;
use App\Repositories\SimulationSchemeRepository;

class SimulationSchemeController extends Controller
{
    protected $produceService;
    protected $simulationRepo;

    public function __construct(GetProduceService $produceService, SimulationSchemeRepository $simulationRepo,
                                 SimulationTransferService $simuService)
    {
        $this->produceService = $produceService;
        $this->simuService = $simuService;
        $this->simulationRepo = $simulationRepo;
        $this->status = ['已生效', '已模擬' , '已確認'];
    }

    public function sourceOrder()
    {
        return view('simulation.source-order');
    }

    public function sourceOrderResult()
    {
        return view('simulation.source-order-result');
    }

    public function getSourceOrderData(SearchSaleOrder $request)
    {
        $data = request([
            'org_id', 'container_date_start',
            'container_date_end', 'bill_date_start',
            'bill_date_end', 'so_id', 'customer_name',
            'amount', 'page','status'
        ]);

        $billStratDate = date('Ymd', strtotime($data['bill_date_start']));
        $billEndDate = date('Ymd', strtotime($data['bill_date_end']));

        $saleOrdeData = $this->produceService->getSourceOrderData(
            $data['org_id'],
            $billStratDate,
            $billEndDate,
            $data['container_date_start'],
            $data['container_date_end'],
            $data['so_id'],
            $data['customer_name']
        );
        return redirect()->route('source-order-result', [
            'org_id' => $data['org_id'],
            'bill_date_start' => $billStratDate,
            'bill_date_end' => $billEndDate,
            'container_date_start' => $data['container_date_start'],
            'container_date_end' => $data['container_date_end'],
            'so_id' => $data['so_id'],
            'customer_name' => $data['customer_name'],
            'status' => $data['status']
        ]);
    }

    public function getManufactureOrderData() //get manufacture
    {
        $datas = request()->only('so_id');
        $this->produceService->getManufactureOrderData($datas['so_id']);

        return view('simulation.manufacture-order', $datas);
    }

    public function manufactureOrderResult()
    {
        return $this->simulationRepo->manufactureOrderResult(request()->amount);
    }

    public function getLoadSourceOrder() //add simulation SO page
    {
        $data = request()->all(
            'bill_date_start',
            'bill_date_end',
            'org_id',
            'container_date_start',
            'container_date_end',
            'customer_name',
            'so_id',
            'status'
        );
        return response()
            ->json($this->simulationRepo->loadedSourceOrder($data)
            ->paginate(request()->amount));
    }

    public function generateSimulation()
    {   
        $datas = request()->only('datas','mo_id');
        $scheme_id = $this->simuService->generateSimulation($datas['datas']);
        return view('simulation.generate-scheme-result')->with(['scheme_id' => $scheme_id]);
    }
    public function getGenerateSimulation()
    {
        return $this->simulationRepo->getGenerateSimulation(request()->only(['amount', 'scheme_id']));
    }
    public function searchScheme()
    {
        return view('simulation.search-scheme');
    }

    public function getSearchScheme()
    {
        $params = request()->only('scheme_id','scheme_status');
        $result = $this->simulationRepo->getSearchScheme($params);
        $status = $this->status[$params['scheme_status']];
        return view('simulation/search-scheme-form')->with(['datas' => json_decode($result),'status'=>$this->status]);
    }

    public function searchSchemeResult($scheme_id)
    {
        return view('simulation.search-scheme-result')->with(['scheme_id' => $scheme_id]);
    }

    public function getSearchSchemeResult()
    {
        # code...
    }
}
