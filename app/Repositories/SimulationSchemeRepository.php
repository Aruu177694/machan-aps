<?php

namespace App\Repositories;

use App\Entities\SimulationManufatureOrder;
use App\Entities\SimulationSourceOrder;
use App\Entities\SimulationInitialScheme;
use App\Services\GetProduceService;

class SimulationSchemeRepository
{
    protected $simuMo;
    protected $simuSo;
    protected $simuInit;

    public function __construct(SimulationManufatureOrder $simuMo, SimulationSourceOrder $simuSo,
                                 SimulationInitialScheme $simuInit)
    {
        $this->simuSo = $simuSo;
        $this->simuMo = $simuMo;
        $this->simuInit = $simuInit;
    }

    public function syncSourceOrder(array $data)
    {
        $batch = SimulationSourceOrder::max('batch') ?: 0;
        collect($data)->each(function ($order) use ($batch) {
            SimulationSourceOrder::firstOrCreate(
                ['so_id' => $order->BillNo],
                [
                    'item' => $order->MaterialId,
                    'org_id' => $order->OrgId,
                    'current_state' => $order->CurrentState,
                    'customer_order' => $order->CustomerOrderNo,
                    'customer_name' => $order->BizPartnerName,
                    'qty' => $order->SQuantity,
                    'container_date' => $order->CU_ContainerDate3 == 0 ? null : $order->CU_ContainerDate3, //結關日期
                    'bill_date' => date('Ymd', strtotime($order->BillDate)),
                    'status' => $order->CU_ScheStatus, //fix APSStatus
                    'person_id' =>$order->PersonId,
                    'material_spec' =>$order->MaterialSpec,
                    'sunit_id' =>$order->SUnitId,
                    'untrans_qty' =>$order->UnTransSQty,
                    'cu_remark' =>$order->CU_Remark2,
                    'cu_ush_date' =>$order->CU_USHdate,
                    'batch' => $batch + 1,
                ]
            );
        });
    }

    public function syncManufactureOrder(array $data)
    {
        collect($data)->each(function ($order) {
            SimulationManufatureOrder::updateOrCreate(
                [
                    'mo_id' => $order->BillNo,
                    'techroutekey_id' => $order->FromTechRouteKeyId
                ],
                [
                    'item_id' => $order->MaterialId,
                    'item_name' => $order->MaterialName,
                    'customer_name' => $order->BizPartnerName,
                    'qty' => $order->ProduceQty,
                    'online_date' => $order->DemandBeginDate, //需求開始日期
                    'so_id' => $order->FromBillNo,
                    'customer' => $order->BizPartnerId,
                    'status' => $order->ProduceState
                ]
            );
        });
    }

    public function manufactureOrderResult($amount) //manufacture-order-result detail
    {
        $so_id = request()->only('so_id');
        $datas = $this->simuMo->whereIn('so_id', $so_id['so_id'])
                         ->orderBy('so_id')
                         ->orderBy('techroutekey_id', 'desc')
                         ->paginate($amount);
        foreach ($datas as $key => $data) {
            $workCenter = $data->relatedTechRoute->workCenter; //get WorkCenter's
            $aps_id = $workCenter->aps_id;
            $resource_id =$workCenter->resource->first()->resource_id; //related resource get resource_id
            $cu_ush_date = $this->simuSo->where('so_id', $data->so_id)->first()->cu_ush_date; // 預設出貨日
            $data->aps_id = $aps_id;
            $data->resource_id = $resource_id;
            $data->cu_ush_date = $cu_ush_date;
        }
        return $datas;
    }

    public function loadedSourceOrder(array $data) //source-order-result
    {
        return SimulationSourceOrder::where('org_id', $data['org_id'])
            ->whereBetween('bill_date', [$data['bill_date_start'], $data['bill_date_end']])->whereIn('status',$data['status'])
            ->when($data['container_date_start'], function ($query, $conStartDate) {
                $query->where('container_date', '>=', $conStartDate);
            })
            ->when($data['container_date_end'], function ($query, $conEndDate) {
                $query->where('container_date', '<=', $conEndDate);
            })
            ->when($data['customer_name'], function ($query, $customerName) {
                $query->where('customer_name', $customerName);
            })
            ->when($data['so_id'], function ($query, $soId) {
                $query->whereIn('so_id', explode(',', $soId));
            });
    }

    public function generateSimulation(array $datas = null, $scheme_id, $batch)
    {
        if ($datas) {
            $this->simuInit->updateOrCreate(
                [
                    'scheme_id' => $scheme_id,
                    'so_id' => $datas['so_id'],
                    'mo_id' => $datas['mo_id'],
                    'item_id' => $datas['item_id']
                ],
                [
                    'qty' => $datas['qty'],
                    'resource_id' => $datas['resource_id'],
                    'aps_id' => $datas['aps_id'],
                    'cu_ush_date' => $datas['cu_ush_date'],
                    'scheme_start' => $datas['BQ'],
                    'scheme_end' => $datas['BU'],
                    'scheme_recommend_lastest_start' => $datas['BY'],
                    'scheme_recommend_lastest_end' => $datas['CK'],
                    'scheme_recommend_early_start' => $datas['CD'],
                    'scheme_recommend_early_end' => $datas['CF'],
                    'scheme_status' => '1',
                    'batch' => $batch
                ]
            );
        } else {
            return 'error datas';
        }
    }

    public function getGenerateSimulation($params)
    {
        return $this->simuInit->where('scheme_id', $params['scheme_id'])->paginate($params['amount']);
    }

    public function getSearchScheme($params)
    {
        return $this->simuInit->select('scheme_id','scheme_status')->groupBy('scheme_id','scheme_status')
                ->where('scheme_id', 'like', "%{$params['scheme_id']}%")->where('scheme_status',$params['scheme_status'])->orderBy('scheme_id', 'asc')->get();
    }

    public function confirmMoStatus($mo_id)
    {
        foreach ($mo_id as $key => $value) 
            SimulationInitialScheme::where('id',$value)->update(['scheme_status' => '2']);

    }
}