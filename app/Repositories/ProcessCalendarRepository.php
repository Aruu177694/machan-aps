<?php

namespace App\Repositories;

use App\Entities\Resource;
use App\Entities\ProcessCalendar;

class ProcessCalendarRepository
{
    public function index($data)
    {
        $apsId = Resource::find($data['id'])->workName->aps_id;
        $resourceInfo = Resource::where('org_id', $data['org_id'])->whereHas('workcenter', function ($query) use ($apsId) {
            $query->where('aps_id', 'like', substr($apsId, 0, 3).'%');
        })->get();
        foreach ($resourceInfo as $key => $resource) {
            $resource->resource_name = $resource->workName->workcenter_name;
        }
        return $resourceInfo;
    }

    public function create(array $data)
    {
        return ProcessCalendar::updateOrCreate(
            ['date' => $data['date'], 'resource_id' => $data['resource_id']],
            [
                'work_type_id' => $data['workId'],
                'status' => $data['status']
            ]
        )->load('setupShift');
    }

    public function show(array $data)
    {
        return ProcessCalendar::whereYear('date', $data['year'])
            ->whereMonth('date', $data['month'])
            ->where('resource_id', $data['resourceId'])
            ->with('setupShift')
            ->get();
    }
}
