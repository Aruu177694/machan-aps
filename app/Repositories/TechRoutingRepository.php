<?php

namespace App\Repositories;

use App\Entities\TechRouting;

class TechRoutingRepository
{
    public function syncTechRouting(array $datas)
    {
        //規格範圍
        $org_id = ['10', '', '20', '30', '', '60', '50'];
        $transfer_factory = ['0', '1', '2', '3', '4', '5', '6'];
        $routing_level = ['11', '20', '30', '40', '50'];
        $routing = ['11', '13', '12', '14'];
        foreach ($datas as $key => $techRouting) {
            if(!isset(
                        $techRouting->TechRouteKeyId,
                        $techRouting->TechRouteKeyName,
                        $techRouting->FactoryId,
                        $techRouting->CU_APSState
            )) continue;
            if ($techRouting->CU_APSState == 0) {
                $tech_route_id = explode('-', $techRouting->TechRouteKeyId);

                if (isset($org_id[$tech_route_id[0][3]])) {
                    $techRouting->org_id = $org_id[$tech_route_id[0][3]];
                    $org_id2 = $org_id[$tech_route_id[0][3]];
                }
                else continue;

                if (isset($transfer_factory[$tech_route_id[1][0]])){
                    $techRouting->transfer_factory = $transfer_factory[$tech_route_id[1][0]];
                }
                else continue;

                $techRouting->factory_type = (int) $techRouting->org_id + (int) $techRouting->transfer_factory;

                if (isset($tech_route_id[2])) {
                    $techRouting->routing_level = $routing[$tech_route_id[2]];
                } else {
                    $techRouting->routing_level = $routing_level[$tech_route_id[1][1]];
                }
                $techRouting->aps_id = $techRouting->routing_level.$techRouting->factory_type;


                TechRouting::updateOrCreate(
                    [
                        'tech_routing_id' => $techRouting->TechRouteKeyId
                    ],
                    [
                        'tech_routing_name' => $techRouting->TechRouteKeyName,
                        'factory_id' => $techRouting->FactoryId,
                        'org_id' => $techRouting->org_id,
                        'transfer_factory' => $techRouting->transfer_factory,
                        'factory_type' => $techRouting->factory_type,
                        'routing_level' => $techRouting->routing_level,
                        'aps_id' => $techRouting->aps_id
                    ]
                );
            }
        }
        return 1;
    }

    public function techRoutingIndex($amount)
    {
        return TechRouting::paginate($amount);
    }

    public function edit($id)
    {
        $data = TechRouting::find($id);

        if (!$data) {
            return false;
        } else {
            return $data;
        }
    }

    public function update(array $params, $id)
    {
        $data = TechRouting::find($id);
        if ($data) {
            return $data->update($params);
        } else {
            return false;
        }
    }
}
