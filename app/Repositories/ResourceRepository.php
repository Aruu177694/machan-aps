<?php

namespace App\Repositories;

use App\Entities\Resource;

class ResourceRepository
{
    public function index($data)
    {
        $resourceInfo = Resource::where('org_id', $data)->get();
        foreach ($resourceInfo as $key => $resource) {
            if ($resource->workName->aps_id) {

                $resource->resource_name = $resource->workName->workcenter_name;
            }
        }

        return $resourceInfo;
    }
}
