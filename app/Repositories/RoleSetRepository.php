<?php

namespace App\Repositories;

use App\Entities\Role;

class RoleSetRepository
{

    public function __construct()
    {
        $this->org = $org = [
            '1000' => '一群',
            '1002' => '二群',
            '1003' => '三群',
            '1009' => '五群',
            '1005' => '六群',
        ];

        $this->routing = $routing = [
            '11' => '鐳射',
            '12' => 'NCT',
            '13' => 'P2',
            '14' => '捲料',
            '20' => '沖床',
            '30' => '點焊',
            '40' => '塗裝',
            '50' => '裝配'
        ];
    }

    public function getRoleSet($amount)
    {
        return Role::paginate($amount);
    }

    public function store($data)
    {
        $name = $this->org[$data['org_select']].'-'.$this->routing[$data['routing_select']];

        Role::create([
            'name' => $name,
            'factory_id' => $data['org_select'],
            'routing_level' => $data['routing_select']
        ]);
    }

    public function find($id)
    {
        return $data = Role::find($id);
    }

    public function destroy($id)
    {
        Role::destroy($id);
    }

    public function update($id, $data)
    {
        $name = $this->org[$data['org_select']].'-'.$this->routing[$data['routing_select']];

        Role::find($id)->update([
            'name' => $name,
            'factory_id' => $data['org_select'],
            'routing_level' => $data['routing_select']
        ]);
    }
}
