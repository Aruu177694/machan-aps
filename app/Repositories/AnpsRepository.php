<?php

namespace App\Repositories;

use App\Entities\SetupAnps;

class AnpsRepository
{
    public function syncANPS(array $datas = null)
    {
        if (!$datas) {
            return '1';
        } else {
            collect($datas)->each(function($data) {
                SetupAnps::updateOrCreate(
                    [
                        "cu_material_id" => $data->CU_MaterialId
                    ],
                    [
                        "bill_no" => $data->BillNo,
                        "cu_org_id" => $data->CU_OrgId,
                        "cu_tht" => $data->CU_THT,
                        "cu_target_prod_qty" => $data->CU_TargetProdQTY,
                        "cu_target_person_qty" => $data->CU_TargetPersonQTY,
                        "cu_ht" => $data->CU_HT,
                        "cu_rw_tht" => $data->CU_RWTHT,
                        "cu_std_ct" => $data->CU_StdCT,
                        "cu_adj_tht" => $data->CU_AdjTHT,
                        "cu_adj_ht" => $data->CU_AdjHT,
                        "cu_packing_tht" => $data->CU_PackingTHT,
                        "cu_adj_packing_tht" => $data->CU_AdjPackingTHT,
                        "cu_wp" => $data->CU_WP,
                        "cu_db_rate" => $data->CU_DBRate
                    ]
                );
            });
            return 0;
        }
    }
}
