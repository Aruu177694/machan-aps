<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class TechRouting extends Model
{
    protected $fillable = [
        'tech_routing_id',
        'tech_routing_name',
        'factory',
        'factory_id',
        'internal_code',
        'status',
        'org_id',
        'transfer_factory',
        'factory_type',
        'routing_level',
        'aps_id',
        'assign_work',
        'standard_time',
        'standard_pre_time',
        'standard_tct',
        'change_time',
        'device_multiple',
        'min_unable_order_time',
        'default_resource'
    ];

    public function workCenter()
    {
        return $this->belongsTo('App\Entities\WorkCenter', 'aps_id', 'aps_id');
    }
}
