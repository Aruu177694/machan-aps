<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $fillable = [
        'factory',
        'factory_name',
        'resource_id',
        'resource_name',
        'resource_type',
        'device_id',
        'device_name',
        'workcenter_id',
        'device_qty',
        'device_multiple',
        'assign_work',
        'standard_time',
        'standard_pre_time',
        'standard_tct',
        'change_time',
        'paint_length',
        'hook_qty',
        'change_color_time',
        'standard_change_time',
        'standard_rate',
        'empty_hook_distance',
        'item',
        'item_name',
        'batch_transfer_setting',
        'org_id',
        'is_default',
    ];

    public function workcenter()
    {
        return $this->hasOne('App\Entities\WorkCenter', 'workcenter_id', 'workcenter_id');
    }

    public function workName()
    {
        return $this->belongsTo('App\Entities\WorkCenter', 'workcenter_id', 'workcenter_id');
    }

    public function shift()
    {
        return $this->belongsTo('App\Entities\SetupShift','assign_work');
    }
}
