<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SimulationInitialScheme extends Model
{
    protected $fillable = [
        'scheme_id', 'so_id', 'mo_id', 'item_id', 'qty', 'resource_id', 'aps_id', 'cu_ush_date', 'scheme_start', 
        'scheme_end', 'scheme_recommend_lastest_start', 'scheme_recommend_lastest_end',
        'scheme_recommend_early_start', 'scheme_recommend_early_end', 'scheme_status','batch'
    ];
}
