<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\GetProduceService;

class GetSourceOrder extends Command
{
    protected $produceService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'source-order:get {start_date?} {end_date?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get source orders from T9 API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(GetProduceService $produceService)
    {
        parent::__construct();
        $this->produceService = $produceService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $startDate = $this->argument('start_date');
        $endDate = $this->argument('end_date');

        if (($startDate && $endDate) || (!$startDate && !$endDate)) {
            $this->produceService->getSourceOrder($startDate, $endDate);
        }
    }
}
