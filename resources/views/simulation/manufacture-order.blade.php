@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    hr {
        border-top: 1px solid #ccc;
    }

    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>初始訂單明細</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">排程模擬</span>
            <span class="space-item">></span>
            <span class="space-item">排程來源載入<span>
            <span class="space-item">></span>
            <span class="space-item">初始訂單明細<span>
            <span class="space-item">></span>
            <span class="space-item">初始模擬製令明細<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>
        </div>
        <div class="total-data">
            <label>班別</label>
            <select name="assign_work" id="assign_work">
            </select>
            <label>預設資源中心</label>
            <select>
                <option value="">-------------</option>
            </select>
            <label>指定資源中心</label>
            <select>
                <option value="">-------------</option>
            </select>
        </div>
        <hr>
        <form action="{{ route('generate-simulation-scheme')}}" method="POST" id='form'>
        @csrf
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos" id="saleorder-data">
                <thead class="thead-color">
                    <tr>
                        <th scope="col"><input type="checkbox" name="check_all" onclick="checkAll(this)"></th>
                        <th scope="col">製令單號</th>
                        <th scope="col">母件</th>
                        <th scope="col">來源訂單號</th>
                        <th scope="col">數量</th>
                        <th scope="col">客戶名稱</th>
                        <th scope="col">預計出貨日</th>
                        <th scope="col">預設班別</th>
                        <th scope="col">預設資源中心</th>
                        <th scope="col">APS製程碼</th>
                        <th scope="col">排單狀態</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div style="text-align:right">
            <span style="display: inline-block; margin-top: 27px;">
                    <span>每頁顯示筆數</span>
                    <select id="amount" onchange="getManufactureOrderData();$('#pagination-demo').twbsPagination('destroy');">
                        <option value="50" selected>50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                    </select>
            </span>
            <ul id="pagination-demo" class="pagination-sm" style="vertical-align: top;"></ul>
        </div>
        <hr>
    </form>
        <div style="text-align:center">
            <button id="sendBtn" class="btn btn-success btn-lg" style="width:45%"  onclick="submit()" > 確認</button>
            <a class="btn btn-secondary btn-lg" href="{{ route('source-order') }}" style="width:45%">返回</a>
        </div>
    </div>
</div>
<script>
    const submit = () => {
        const e = $(':checkbox');
        if(e.filter(function(){
            return this.checked == true
        }).length) $('#form').submit();
        else alert('請至少選擇一個項目');

    }
    const checkAll = (I) => {
      const e = $(':checkbox');
      for (var i = e.length - 1; i >= 0; i--) {
        if(I.checked == true) e[i].checked = true ;
        else e[i].checked = false ;
      }
    }
    const checkGroup = (ckAll,group) => {
      var e = $(`input[group^='${group}']`);
      for (var i=0;i<e.length;i++)
        e[i].checked = ckAll.checked;
    }
    let lastPage;
    const getManufactureOrderData = (page = 1) => {
        const amount = $('#amount').val();
        axios.get('{{ route('manufacture-order-result') }}' + location.search, {
            params: {
                amount,
                page,
            }
        }).then(({ data }) => {
            lastPage = data.last_page;
            const orders = data.data;
            $('#saleorder-data tbody').empty();
            const status_list = ['','已生效'] ;
            let id = 0;
            orders.forEach((order, key) => {
                let status = status_list[Number(order.status)] ;
                $('#saleorder-data tbody').append(`
                    <tr>
                        <td scope="row">
                            <input type="checkbox" name="mo_id[${key}]" value="${id}" onclick="checkGroup(this,${key})" group='${key}'>
                        <td>
                            <input type="checkbox" name="datas[${key}][mo_id]" value="${order.mo_id}"  group='${key}' hidden>${order.mo_id}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][item_id]" value="${order.item_id}"  group='${key}' hidden>${order.item_id}
                        </td>
                        <td>${order.so_id}</td>
                        <td>
                            <input type="checkbox" name="datas[${key}][qty]" value="${order.qty}"  group='${key}' hidden>${order.qty}
                        </td>
                        <td>${order.customer_name}</td>
                        <td>
                            <input type="checkbox" name="datas[${key}][cu_ush_date]" value="${order.cu_ush_date}"  group='${key}' hidden>${order.cu_ush_date}
                        </td>
                        <td>正常班</td>
                        <td>
                            <input type="checkbox" name="datas[${key}][resource_id]" value="${order.resource_id}"  group='${key}' hidden>${order.resource_id}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][aps_id]" value="${order.aps_id}"  group='${key}' hidden>${order.aps_id}
                        </td>
                        <td>${status}</td>
                    </tr>
                `)
            });

            $('#pagination-demo').twbsPagination({
                totalPages: lastPage,
                visiblePages: 5,
                first:'頁首',
                last:'頁尾',
                prev:'<',
                next:'>',
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    getManufactureOrderData(page)
                }
            });
        });
        axios.get('{{route('work-type-data')}}',{
            params: {
                amount,
                page,
            }     
        })
          .then(res => {
            let shift = res.data.data;
            console.log(shift);
            shift.forEach((item)=>{
                $('#assign_work').append(`<option value="${item.name}">${item.name}</option>`);
            })

          })
    }
    getManufactureOrderData();
</script>
@endsection
