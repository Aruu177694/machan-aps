@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    hr {
        border-top: 1px solid #ccc;
    }

    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>初始訂單明細</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">排程模擬</span>
            <span class="space-item">></span>
            <span class="space-item">排程來源載入<span>
            <span class="space-item">></span>
            <span class="space-item">初始訂單明細<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>
        </div>
        <div class="total-data">
            載入筆數 |
            <span id="data-num"></span>
            <select name="" id="" style="float:right;">
                <option value="">標準初始模擬</option>
                <option value="">標準模擬</option>
                <option value="">最佳化模擬</option>
                <option value="">標準最佳化模擬</option>
            </select>
            <label style="float:right;">選擇排單模擬計畫&emsp;</label>
        </div>
        <hr>
        <div class="total-data">
            <div style="float:right; margin-bottom:10px">
                <button  class="btn btn-success">新增</button>
                <button  class="btn btn-danger">刪除</button>
            </div>
        </div>
        <form action="{{ route('get-manufacture-order')}}" method="GET" id='form'>
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos" id="saleorder-data">
                <thead class="thead-color">
                    <tr>
                        <th scope="col"><input type="checkbox" name="check_all" onclick="checkAll(this)"></th>
                        <th scope="col">序</th>
                        <th scope="col">訂單單號</th>
                        <th scope="col">品號</th>
                        <th scope="col">客戶名稱</th>
                        <th scope="col">客戶單號</th>
                        <th scope="col">數量</th>
                        <th scope="col">結關日</th>
                        <th scope="col">排單狀態</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div style="text-align:right">
            <span style="display: inline-block; margin-top: 27px;">
                    <span>每頁顯示筆數</span>
                    <select id="amount" onchange="getSaleOrderData();$('#pagination-demo').twbsPagination('destroy');">
                        <option value="50" selected>50</option>
                        <option value="100">100</option>
                        <option value="200">200</option>
                    </select>
            </span>
            <ul id="pagination-demo" class="pagination-sm" style="vertical-align: top;"></ul>
        </div>
        <hr>
    </form>
        <div style="text-align:center">
            <button id="sendBtn" class="btn btn-success btn-lg" style="width:45%"  onclick="submit()" > 確認</button>
            <a class="btn btn-secondary btn-lg" href="{{ route('source-order') }}" style="width:45%">返回</a>
        </div>
    </div>
</div>
<script>
    let lastPage;
    const submit = () => {
        const e = $(':checkbox');
        if(e.filter(function(){
            return this.checked == true
        }).length) $('#form').submit();
        else alert('請至少勾取一個項目');

    }
    const checkAll = (I) => {
      const e = $(':checkbox');
      for (var i = e.length - 1; i >= 0; i--) {
        if(I.checked == true) e[i].checked = true ;
        else e[i].checked = false ;
      }
    }
    const getSaleOrderData = (page = 1) => {
        const amount = $('#amount').val();
        axios.get('{{ route('load-source-order') }}' + location.search, {
            params: {
                amount,
                page,
            }
        }).then(({ data }) => {
            lastPage = data.last_page;
            const orders = data.data;
            $('#data-num').text(`共 ${data.total} 筆`);
            $('#saleorder-data tbody').empty();

            $('#checkAll')
            orders.forEach((order, key) => {
                $('#saleorder-data tbody').append(`
                    <tr>
                        <td scope="row">
                            <input type="checkbox" name="so_id[]" value="${order.so_id}"/>
                        </td>
                        <th scope="row">${key + 1 + (page - 1) * amount}</th>
                        <td>
                            ${order.so_id}
                        </td>
                        <td>${order.item}</td>
                        <td>${order.customer_name}</td>
                        <td>${order.customer_order}</td>
                        <td>${order.qty}</td>
                        <td>${order.container_date}</td>
                        <td>已生效</td>
                    </tr>
                `)
            });

            $('#pagination-demo').twbsPagination({
                totalPages: lastPage,
                visiblePages: 5,
                first:'頁首',
                last:'頁尾',
                prev:'<',
                next:'>',
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    getSaleOrderData(page)
                }
            });
        });
    }
    getSaleOrderData();
</script>
@endsection
