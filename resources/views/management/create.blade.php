@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
    .form-horizontal .control-label {
        text-align: center;
    }
    hr {
        border-top: 1px solid #ccc;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    .btn-secondary:hover {
        color: #fff;
        background-color: #5a6268;
        border-color: #545b62;
    }
    .btn.focus, .btn:focus, .btn:hover {
        color: #fff;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>人員帳號管理</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">系統管理</span>
            <span class="space-item">></span>
            <span class="space-item">人員帳號管理<span>
            <span class="space-item">></span>
            <span class="space-item">編輯帳號資訊</span>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">資料編輯</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{ route('management.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-2 control-label">人員編號</label>
                                <div class="col-md-10">
                                <input type="text" class="form-control" name="account" required value="">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">人員名稱</label>
                                <div class="col-md-10">
                                <input type="text" class="form-control" name="name" required value="">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">所屬組織</label>
                                <div class="col-md-10">
                                    <select class="form-control" id="organization_sel" name="factory_id" required value="">
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">歸屬角色</label>
                                <div class="col-md-10">
                                    <select class="form-control" id="role_sel" name="role_id" required value="">
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div style="text-align:center">
                                <input type="submit" id="sendBtn" class="btn btn-success btn-lg" style="width:45%" value="確認">
                                <a class="btn btn-secondary btn-lg" href="{{ route('management.index') }}" style="width:45%">返回</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    const getRole = () => {
            axios.get('{{ route('getRole') }}', {})
        .then(({ data }) => {
            $('#role_sel').empty();
            data.forEach(data => {
                    $('#role_sel').append(`
                        <option value="${data.id}">${data.name}</option>
                    `);
                })
        });
    }
    const getOrganization = () => {
            axios.get('{{ route('getOrganization') }}', {})
        .then(({ data }) => {
            $('#organization_sel').empty();
            data.forEach(data => {
                    $("#organization_sel").append(`
                        <option value="${data.factory_id}">${data.name}</option>
                    `);
                })
        });
    }
    getRole();
    getOrganization();
</script>

@endsection
