@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
        /* padding: 0px 10px; */
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>工作中心設定</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">系統管理</span>
            <span class="space-item">></span>
            <span class="space-item">工作中心設定<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>
            <div style="float:right; margin-top:-7px">
                    <a href="{{ route('getCenterData') }}" class="btn btn-success">同步更新</a>
            </div>
        </div>
        <div class="total-data">
            載入筆數 |
            <span id="data-num"></span>
        </div>
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos" id="workcenter-table">
                <thead class="thead-color">
                    <tr>
                        <th scope="col">序</th>
                        <th scope="col">工廠</th>
                        <th scope="col">工作中心代碼</th>
                        <th scope="col">工作中心名稱</th>
                        <th scope="col">事業群</th>
                        <th scope="col">轉廠</th>
                        <th scope="col">廠別</th>
                        <th scope="col">工作階</th>
                        <th scope="col">製程階</th>
                        <th scope="col">APS製程碼</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div style="text-align:right">
            <span style="display: inline-block; margin-top: 27px;">
                    <span>每頁顯示筆數</span>
                    <select id="amount" onchange="getIndexData();$('#pagination-demo').twbsPagination('destroy');">
                        <option value="10" selected>10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </select>
            </span>
            <ul id="pagination-demo" class="pagination-sm" style="vertical-align: top;"></ul>
        </div>
    </div>
</div>

<script>
    if(!{{$status}}) {
        alert('t9 problem');
    }
    let lastPage;
    const getIndexData = (page = 1) => {
        const amount = $('#amount').val();
        axios.get('{{ route('getIndexData') }}',{
            params: {
                amount,
                page,
            }
        }).then(({ data }) => {
            lastPage = data.last_page;

            var org_id = [];
                org_id['10'] = '一群';
                org_id['20'] = '二群';
                org_id['30'] = '三群';
                org_id['40'] = '四群';
                org_id['50'] = '五群';
                org_id['60'] = '六群';
            var transfer = [];
                transfer['0'] = '自製';
                transfer['1'] = '轉一廠';
                transfer['2'] = '轉二廠';
                transfer['3'] = '轉三廠';
                transfer['4'] = '轉四廠';
                transfer['5'] = '轉五廠';
                transfer['6'] = '轉六廠';
                transfer[''] = '';
            var routing = [];
                routing['11'] = '鐳射';
                routing['12'] = 'NCT';
                routing['13'] = 'P2';
                routing['14'] = '捲料機';
                routing['20'] = '沖床';
                routing['30'] = '點焊';
                routing['40'] = '塗裝';
                routing['50'] = '裝配';
                routing[''] = '';

            const orders = data.data;
            $('#data-num').text(`共 ${data.total} 筆`);
            $('#workcenter-table tbody').empty();
            orders.forEach((order, key) => {
                $('#workcenter-table tbody').append(`
                    <tr>
                        <th scope="row">${key + 1 + (page - 1) * amount}</th>
                        <td>${order.factory_id}</td>
                        <td>${order.workcenter_id}</td>
                        <td>${order.workcenter_name}</td>
                        <td>${org_id[order.org_id]}</td>
                        <td>${transfer[order.transfer_factory]}</td>
                        <td>${order.factory_type}</td>
                        <td>${order.work_level}</td>
                        <td>${routing[order.routing_level]}</td>
                        <td>${order.aps_id}</td>
                    </tr>
                `)
             });
            $('#pagination-demo').twbsPagination({
                totalPages: lastPage,
                visiblePages: 5,
                first:'頁首',
                last:'頁尾',
                prev:'<',
                next:'>',
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    getIndexData(page)
                }
            });
        });
    }
    getIndexData();
</script>
@endsection
