@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
    .form-horizontal .control-label {
        text-align: center;
    }
    hr {
        border-top: 1px solid #ccc;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    .btn-secondary:hover {
        color: #fff;
        background-color: #5a6268;
        border-color: #545b62;
    }
    .btn.focus, .btn:focus, .btn:hover {
        color: #fff;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>資源設定</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">系統管理</span>
            <span class="space-item">></span>
            <span class="space-item">資源設定<span>
            <span class="space-item">></span>
            <span class="space-item">編輯資源設定</span>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">資料編輯</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{ route('resource.update', $data->id) }}" method="POST">
                            @csrf
                            @method("PUT")
                            <div class="form-group">
                                <label class="col-md-2 control-label">資源代碼</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="resource_id" required value="{{ $data->resource_id }}">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">資源名稱</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="resource_name" required value="{{ $data->resource_name }}">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">資源類別</label>
                                <div class="col-md-10">
                                    <select id="resoruce_type" name="resource_type" class="form-control" required>
                                        <option disabled selected value="">--- 請選擇資源類別 ---</option>
                                        <option value="設備" {{ $data->resource_type === '設備' ? 'selected' : '' }}>設備</option>
                                        <option value="人力" {{ $data->resource_type === '人力' ? 'selected' : '' }}>人力</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">設備編碼</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="device_id" required value="{{ $data->device_id }}">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">機台名稱/規格</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="device_name" required value="{{ $data->device_name }}">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">機台數量</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="device_qty" required value="{{ $data->device_qty }}">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">製程類別</label>
                                <div class="col-md-10">
                                    <select id="routing_select" name="routing_select" class="form-control" onchange="getCenter()" required>
                                        <option value="11" {{ $data->workcenter->routing_level === '11' ? 'selected' : '' }}>鐳射</option>
                                        <option value="12" {{ $data->workcenter->routing_level === '12' ? 'selected' : '' }}>NCT</option>
                                        <option value="13" {{ $data->workcenter->routing_level === '13' ? 'selected' : '' }}>P2</option>
                                        <option value="20" {{ $data->workcenter->routing_level === '20' ? 'selected' : '' }}>沖床</option>
                                        <option value="30" {{ $data->workcenter->routing_level === '30' ? 'selected' : '' }}>點焊</option>
                                        <option value="40" {{ $data->workcenter->routing_level === '40' ? 'selected' : '' }}>塗裝</option>
                                        <option value="50" {{ $data->workcenter->routing_level === '50' ? 'selected' : '' }}>裝配</option>
                                        <option value=""   {{ $data->workcenter->routing_level === '' ? 'selected' : '' }}>其它</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">工作中心代碼</label>
                                <div class="col-md-10">
                                    <select class="form-control" id="workcenter_sel" name="workcenter_id" required>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">預設班別</label>
                                <div class="col-md-10">
                                    <select type="text" class="form-control" name="assign_work" id="assign_work"> </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">標準前置(小時)</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="standard_pre_time" value="{{ $data->standard_pre_time }}">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">標準生產時間(小時)</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="standard_time" value="{{ $data->standard_time }}">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                    <label class="col-md-2 control-label">標準TCT(秒)</label>
                                    <div class="col-md-10">
                                        <input type="text" class="form-control" name="standard_tct" value="{{ $data->standard_tct }}">
                                    </div>
                                </div>
                                <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">標準換線(分)</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="change_time" value="{{ $data->change_time }}">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">設備人力倍數</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="device_multiple" value="{{ $data->device_multiple }}">
                                </div>
                            </div>
                            <hr>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">是否為預設資源中心</label>
                                    <div class="col-md-10">
                                        <select id="is_default" name="is_default" class="form-control" required>
                                            <option value="0" {{ $data->is_default === 0 ? 'selected' : '' }}>否</option>
                                            <option value="1" {{ $data->is_default === 1 ? 'selected' : '' }}>是</option>
                                        </select>
                                    </div>
                                </div>
                            <hr>
                            <div class="form-group" id="painting">
                            </div>
                            <hr>
                            <div style="text-align:center">
                                <input type="submit" id="sendBtn" class="btn btn-success btn-lg" style="width:45%" value="確認">
                                <a class="btn btn-secondary btn-lg" href="{{ route('resource.index') }}" style="width:45%">返回</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    let a = true;
    const getShift = () => {
        axios.get('{{route('get-work-time')}}')
        .then(({data}) => {
            console.log(data);
            $("#assign_work").append(`<option disabled selected value="{{$data->assign_work}}">{{$shift}}</option>`)
            data.forEach(data => {
                $("#assign_work").append(`<option value="${data.id}">${data.name}</option>`)
            })
        });
    }
    const getCenter = () => {
        axios.get('{{ route('getcenter') }}', {
            params: {
                value: $('#routing_select').val(),
            }
        })
        .then(({ data }) => {
            $("#workcenter_sel").empty();
            if (a) {
                $("#workcenter_sel").append(`Z
                    <option disabled selected value="{{ $data->workcenter_id }}">{{ $data->workcenter_id }}</option>
                `)
                a = !a
            }
            data.forEach(data => {
                $("#workcenter_sel").append(`
                    <option value="${data.workcenter_id}">${data.workcenter_id}</option>
                `);
            })
        });

        if($('#routing_select').val()==40) {
            $('#painting').append(`
                <label class="col-md-2 control-label">烤漆線總長(公尺)</label>
                <div class="col-md-10" style="margin-bottom: 15px;">
                    <input type="text" class="form-control" name="paint_length" required value="{{ $data->paint_length }}">
                </div>
                <label class="col-md-2 control-label">總鉤數</label>
                <div class="col-md-10" style="margin-bottom: 15px;">
                    <input type="text" class="form-control" name="hook_qty" required value="{{ $data->hook_qty }}">
                </div>
                <label class="col-md-2 control-label">換色時間(分)</label>
                <div class="col-md-10" style="margin-bottom: 15px;">
                    <input type="text" class="form-control" name="change_color_time" required value="{{ $data->change_color_time }}">
                </div>
                <label class="col-md-2 control-label">標準換模時間(分)</label>
                <div class="col-md-10" style="margin-bottom: 15px;">
                    <input type="text" class="form-control" name="standard_change_time" required value="{{ $data->standard_change_time }}">
                </div>
                <label class="col-md-2 control-label">標準速率(D/T)M/min</label>
                <div class="col-md-10" style="margin-bottom: 15px;">
                    <input type="text" class="form-control" name="standard_rate" required value="{{ $data->standard_rate }}">
                </div>
                <label class="col-md-2 control-label">完工空勾距離</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="empty_hook_distance" required value="{{ $data->empty_hook_distance }}">
                </div>
            `);
        }
        else {
            $('#painting').empty();
        }
    }
    getShift();
    getCenter();
</script>

@endsection
