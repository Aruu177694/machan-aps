@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
    .form-horizontal .control-label {
        text-align: center;
    }
    hr {
        border-top: 1px solid #ccc;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    .btn-secondary:hover {
        color: #fff;
        background-color: #5a6268;
        border-color: #545b62;
    }
    .btn.focus, .btn:focus, .btn:hover {
        color: #fff;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>工藝路線設定</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">系統管理</span>
            <span class="space-item">></span>
            <span class="space-item">工藝路線設定<span>
            <span class="space-item">></span>
            <span class="space-item">編輯工藝路線</span>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">資料編輯</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{ route('tech-routing.update', $datas->id) }}" method="POST">
                            @csrf
                            @method("PUT")
                            <div class="form-group">
                                <label class="col-md-2 control-label">工藝路線代碼</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="tech_routing_id" value="{{ $datas->tech_routing_id }}" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">工藝路線名稱</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="tech_routing_name" value="{{ $datas->tech_routing_name }}" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">預設班別</label>
                                <div class="col-md-4">
                                    <select id="assign_work" name="assign_work" class="form-control" required>
                                        <option disabled selected value="">--- 請選擇預設班別 ---</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" class="form-control" name="">
                                </div>
                            </div>
                            <hr>

                            <div class="form-group">
                                <label class="col-md-2 control-label">標準前置(小時)</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="standard_pre_time"value="{{ $datas->standard_pre_time }}">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">標準TCT(秒)</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="standard_tct" value="{{ $datas->standard_tct }}">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">標準換線(分)</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="change_time" value="{{ $datas->change_time }}">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">設備人力倍數</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="device_multiple" value="{{ $datas->device_multiple }}">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">最小無法排單<br>時間單位(分)</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="min_unable_order_time" value="{{ $datas->min_unable_order_time }}" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">預設資源中心</label>
                                <div class="col-md-10">
                                    <select id="default_resource" name="default_resource" class="form-control" required>
                                        <option disabled selected value="">--- 請選擇預設資源中心 ---</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div style="text-align:center">
                                <button type="submit" id="sendBtn" class="btn btn-success btn-lg" style="width:45%">確認</button>
                                <a class="btn btn-secondary btn-lg" href="{{ route('tech-routing.index') }}" style="width:45%">返回</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    const getWorkTypeData = () => {
        axios.get('{{ route('work-type-data') }}',{
        })
        .then(({ data }) => {
            const orders = data.data;
            orders.forEach(data => {
                $("#assign_work").append(`
                    <option value="${data.id}">${data.type}</option>
                `);
            });
        });
    }
    const getResource = () => {
        axios.get('{{ route('getResource') }}', {
        })
        .then(({ data }) => {
            data.forEach(data => {
                $('#default_resource').append(`
                    <option value="${data.resource_id}">${data.resource_id}</option>
                `);
            });
        });
    }
    getWorkTypeData();
    getResource();
</script>

@endsection
