@extends('layouts.myapp')

@section('css')
<style>
    .box__dragndrop,
    .box__uploading,
    .box__success,
    .box__error {
    display: none;
    }
    .block {
        border: dashed 1px;
        text-align: center;
      }
    .space-item {
        margin-left: 10px;
    }
    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
    .form-horizontal .control-label {
        text-align: center;
    }
    hr {
        border-top: 1px solid #ccc;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    .btn-secondary:hover {
        color: #fff;
        background-color: #5a6268;
        border-color: #545b62;
    }
    .btn.focus, .btn:focus, .btn:hover {
        color: #fff;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>匯入生產進度表</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">模擬排單</span>
            <span class="space-item">></span>
            <span class="space-item">匯入生產進度表<span>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">資料搜尋</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{ route('compare-data') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-2 control-label">組織</label>
                                <div class="col-md-5">
                                    <select class="form-control" id="sel1" name="org_id" required>
                                        <option selected>--- 請選擇組織 ---</option>
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select class="form-control" id="sel2" name="process_type">
                                        <option selected>--- 請選擇製程別 ---</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">年月</label>
                                <div class="col-md-10">
                                    <input id="sel3" name="date" class="form-control" type="month" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-xs-2 control-label">排程表</label>
                                <div class="col-xs-9 block" style="height:25vh; margin-left:3vh">
                                    <div style="height: 5vh;"></div>
                                    <h4 style="color:gray;">請將檔案拖曳到此.....</h4>
                                    <p style="color:gray;">或</p>
                                    <input class="btn btn-default" name="file" type="file" style="display: inline-block;">
                                    @if ($errors->has('file'))
                                        <div class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('file') }}</strong>
                                        </div>
                                    @endif
                                </div>
                            </div>
                            <hr>
                            <div style="text-align:center">
                                <button type="submit" id="sendBtn" class="btn btn-success btn-lg" style="width:45%">上傳檔案</button>
                                <button type="reset" onclick="resetOption()" class="btn btn-secondary btn-lg" style="width:45%">清除</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    const getOrganization = () => {
        axios.get('{{ route('getorganization') }}')
            .then(({ data }) => {
                data.forEach(data => {
                    $('#sel1').append(`
                        <option value="${data.factory_id}">${data.name}</option>
                    `);
                });
            });
    }

    // const getResource = () => {
    //     axios.get('{{ route('getresource') }}', {
    //         params: {
    //             value: $('#sel1').val(),
    //         }
    //     })
    //     .then(({ data }) => {
    //         $('#sel2').empty();
    //         $('#sel2').append(`
    //             <option disabled selected value="">--- 請選擇 ---</option>
    //         `)
    //         data.forEach(data => {
    //             $('#sel2').append(`
    //                 <option value="${data.id}">${data.resource_name}</option>
    //             `);
    //         })
    //     });
    // }

    // const resetOption = () => {
    //     $('#sel2').empty();
    //     $('#sel2').append(`
    //         <option disabled selected value="">--- 請選擇製程別 ---</option>
    //     `)
    // }
    getOrganization();
</script>
@endsection
