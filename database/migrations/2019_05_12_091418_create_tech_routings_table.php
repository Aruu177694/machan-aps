<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTechRoutingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tech_routings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tech_routing_id');
            $table->string('tech_routing_name');
            $table->string('factory')->default('1000');
            $table->string('factory_id');
            $table->string('internal_code')->default('001');
            $table->string('status')->default('2');
            $table->string('org_id');
            $table->string('transfer_factory');
            $table->string('factory_type');
            $table->string('routing_level');
            $table->string('aps_id');
            $table->unsignedInteger('assign_work')->nullable();
            $table->float('standard_time')->nullable();
            $table->float('standard_pre_time')->nullable();
            $table->unsignedInteger('standard_tct')->nullable();
            $table->unsignedInteger('change_time')->nullable();
            $table->unsignedInteger('device_multiple')->nullable();
            $table->unsignedInteger('min_unable_order_time')->nullable();
            $table->string('default_resource')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tech_routings');
    }
}
