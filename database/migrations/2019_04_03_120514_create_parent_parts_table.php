<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateParentPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('parent_parts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('material_id')->nullable();
            $table->string('bomkey_id')->nullable();
            $table->string('bomkey_name')->nullable();
            $table->string('unit_id')->nullable();
            $table->string('techroutekey_id')->nullable();
            $table->unsignedInteger('fetch_type')->nullable()->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('parent_parts');
    }
}
